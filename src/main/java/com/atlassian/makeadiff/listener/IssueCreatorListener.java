package com.atlassian.makeadiff.listener;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.event.type.EventType;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.makeadiff.api.FoundationProject;
import com.atlassian.makeadiff.api.FoundationProjectService;


//The IssueCreatorListener listens for the creation of a task from the add task form.
//It then updates the project status to In Progress if it is not already at that stage.
public class IssueCreatorListener
{

    private final FoundationProjectService projectService;
    private final JiraAuthenticationContext authContext;

    public IssueCreatorListener(EventPublisher eventPublisher, FoundationProjectService project, JiraAuthenticationContext authContext)
    {
        eventPublisher.register(this);
        this.projectService = project;
        this.authContext = authContext;
    }

    @EventListener
    public void onIssueCreation(IssueEvent event)
    {
        if (event.getEventTypeId().equals(EventType.ISSUE_CREATED_ID) && event.getProject().getProjectLead().equals(authContext.getLoggedInUser()))
        {
            FoundationProject project = projectService.getProject(event.getProject().getKey());
            if (project.getStatus().equals(FoundationProject.Status.NEW))
            {
                project.setStatus(FoundationProject.Status.IN_PROGRESS);
            }
            //TODO: Add code to update the project

        }
    }

    @EventListener
    public void onIssueUpdate(IssueEvent event)
    {
        if (event.getEventTypeId().equals(EventType.ISSUE_UPDATED_ID) && event.getProject().getProjectLead().equals(authContext.getLoggedInUser()))
        {
            //TODO: Add code to update the project status ONLY if the user trying to change it is authorised
//            if (projectService.getProject(event.getProject().getKey()).getStatus())
        }
    }

}