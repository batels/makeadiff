package com.atlassian.makeadiff.filter;

import com.atlassian.core.filters.AbstractHttpFilter;
import com.atlassian.jira.plugin.webresource.JiraWebResourceManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.JiraKeyUtils;
import com.atlassian.makeadiff.api.FoundationProject;
import com.atlassian.makeadiff.api.FoundationProjectService;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Filter to show the project page.
 */
public class ProjectPageFilter extends AbstractHttpFilter
{
    private static final Logger log = LoggerFactory.getLogger(ProjectPageFilter.class);

    public static final String OVERVIEW = "overview";
    public static final String TASKS = "tasks";
    public static final String CONTRIBUTORS = "contributors";
    private final FoundationProjectService foundationProjectService;
    private final JiraWebResourceManager webResourceManager;
    private final PageBuilderService pageBuilderService;
    private final JiraAuthenticationContext authContext;
    private final PermissionManager permissionManager;

    public ProjectPageFilter(final FoundationProjectService foundationProjectService,
        final JiraWebResourceManager webResourceManager, final PageBuilderService pageBuilderService,
        JiraAuthenticationContext authContext, PermissionManager permissionManager)
    {
        this.foundationProjectService = foundationProjectService;
        this.webResourceManager = webResourceManager;
        this.pageBuilderService = pageBuilderService;
        this.authContext = authContext;
        this.permissionManager = permissionManager;
    }

    @Override
    protected void doFilter(final HttpServletRequest request, final HttpServletResponse response, final FilterChain filterChain)
        throws IOException, ServletException
    {
        final FoundationProject project = getProject(request);
        if (project == null)
        {
            log.error("ProjectPageFilter: project is null. projectKey=" + getProjectKey(request) + ". requestURL=" + request.getRequestURL());
            filterChain.doFilter(request, response);
            return;
        }

        final ApplicationUser user = authContext.getUser();
        if (!permissionManager.hasPermission(Permissions.BROWSE, project, user))
        {
            response.sendRedirect(request.getContextPath() + "/");
            return;
        }

        String currentPage = getCurrentPage(request);
        if (StringUtils.isBlank(currentPage))
        {
            currentPage = OVERVIEW;
        }

        final boolean hasCreateIssuePermission = permissionManager.hasPermission(Permissions.CREATE_ISSUE, project, user);
        final boolean hasProjectAdminPermission = permissionManager.hasPermission(Permissions.PROJECT_ADMIN, project, user);

        pageBuilderService.assembler().data().requireData("makeadiff-project.key", project.getKey());
        pageBuilderService.assembler().data().requireData("makeadiff-project.name", project.getName());
        pageBuilderService.assembler().data().requireData("makeadiff-project.id", project.getId());
        pageBuilderService.assembler().data().requireData("makeadiff-project.hasCreateIssuePermission", hasCreateIssuePermission);
        pageBuilderService.assembler().data().requireData("makeadiff-project.hasProjectAdminPermission", hasProjectAdminPermission);
        pageBuilderService.assembler().data().requireData("makeadiff-project.page", currentPage);


        if (OVERVIEW.equalsIgnoreCase(currentPage))
        {
            request.getRequestDispatcher(
                request.getContextPath() + "/secure/BrowseFoundationProject.jspa?key=" + project.getKey() +
                    "&selectedTab=overview").forward(request, response);

        }
        else if (TASKS.equalsIgnoreCase(currentPage))
        {
            if (project.getAgileBoardId() != null)
            {
                request.setAttribute("agileBoardId", project.getAgileBoardId());
                webResourceManager.requireResource("com.atlassian.makeadiff.makeadiff-jira-plugin:project-web-resources");
                webResourceManager.requireResource("com.atlassian.makeadiff.makeadiff-jira-plugin:project-tasks-web-resources");
                final String forwardUrl = request.getContextPath() + "/secure/RapidBoard.jspa?rapidView=" + project.getAgileBoardId();
                request.getRequestDispatcher(forwardUrl).forward(request, response);
            }
            else
            {
                throw new RuntimeException("This project is not properly set up. It has no tasks board");
            }
        }
        else if (CONTRIBUTORS.equalsIgnoreCase(currentPage))
        {
            request.getRequestDispatcher(
                request.getContextPath() + "/secure/BrowseFoundationProject.jspa?key=" + project.getKey() +
                    "&selectedTab=contributors").forward(request, response);
        }

    }

    private FoundationProject getProject(final HttpServletRequest request)
    {
        final String key = getProjectKey(request);
        return (key == null) ? null : foundationProjectService.getProject(key);
    }

    private String getProjectKey(final HttpServletRequest request)
    {
        final String pathInfo = getPathInfo(request);
        if (pathInfo != null && !pathInfo.isEmpty())
        {
            if (!JiraKeyUtils.validIssueKey(pathInfo))
            {
                if (!pathInfo.contains("/"))
                {
                    return pathInfo;
                }
                else
                {
                    return pathInfo.substring(0, pathInfo.indexOf("/"));
                }
            }
        }

        return null;
    }

    private String getCurrentPage(final HttpServletRequest request)
    {
        final String pathInfo = getPathInfo(request);
        if (pathInfo != null && !pathInfo.isEmpty())
        {
            String[] pathParts = pathInfo.split("/");
            if (pathParts.length > 1)
            {
                return pathParts[1];
            }
        }
        return null;
    }

    private static String getPathInfo(final HttpServletRequest request)
    {
        final String pathInfo = request.getPathInfo();
        if (pathInfo == null)
        {
            return null;
        }

        return removeLeadingSlash(removeTrailingSlash(pathInfo));
    }

    private static String removeLeadingSlash(final String s)
    {
        if (s.startsWith("/"))
        {
            return s.substring(1);
        }

        return s;
    }

    private static String removeTrailingSlash(final String s)
    {
        if (s.endsWith("/"))
        {
            return s.substring(0, s.length() - 1);
        }

        return s;
    }
}
