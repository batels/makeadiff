package com.atlassian.makeadiff.filter;

import com.atlassian.core.filters.AbstractHttpFilter;
import com.atlassian.makeadiff.api.ProjectImageService;
import org.apache.commons.lang.StringUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;

public class TempImageFilter extends AbstractHttpFilter
{
    private final ProjectImageService projectImageService;

    public TempImageFilter(final ProjectImageService projectImageService)
    {
        this.projectImageService = projectImageService;
    }

    @Override
    public void doFilter(final HttpServletRequest request, final HttpServletResponse response, final FilterChain filterChain) throws IOException, ServletException
    {
        final String tempFileId = getTempFileId(request);
        if (StringUtils.isBlank(tempFileId))
        {
            response.sendError(HttpServletResponse.SC_NOT_FOUND, "Missing argument: tempFileId");
        }

        boolean bytesWritten = false;
        try
        {
            final File imageFile = projectImageService.getTempImage(tempFileId);
            if (imageFile == null)
            {
                response.sendError(HttpServletResponse.SC_NOT_FOUND, "Image not found");
            }
            else
            {
                ImageFilterHelper.sendImage(response, imageFile);
                bytesWritten = true;
            }
        }
        catch (IOException e)
        {
            // TODO
            //handleOutputStreamingException(response, bytesWritten, e);
        }
        catch (RuntimeException e)
        {
            // TODO
            //handleOutputStreamingException(response, bytesWritten, e);
        }
    }

    private String getTempFileId(final HttpServletRequest request)
    {
        return StringUtils.trim(request.getParameter("tempFileId"));
    }
}
