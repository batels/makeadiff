package com.atlassian.makeadiff.filter;

import com.atlassian.core.filters.AbstractHttpFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * Filter to redirect to the home page.
 */
public class RedirectToProfilePageFilter extends AbstractHttpFilter
{
    @Override
    protected void doFilter(final HttpServletRequest request, final HttpServletResponse response, final FilterChain filterChain)
            throws IOException, ServletException
    {
        String queryString = request.getQueryString();
        String redirectUrl = "/view/profile";
        if (isNotBlank(queryString))
            redirectUrl += "?" + queryString;
        response.sendRedirect(request.getContextPath() + redirectUrl);
    }
}
