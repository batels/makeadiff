package com.atlassian.makeadiff.filter;

import com.atlassian.core.filters.AbstractHttpFilter;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Filter to show the profile page.
 */
public class ProfilePageFilter extends AbstractHttpFilter
{
    private final UserManager userManager;

    public ProfilePageFilter(UserManager userManager)
    {
        this.userManager = userManager;
    }

    @Override
    protected void doFilter(final HttpServletRequest request, final HttpServletResponse response, final FilterChain filterChain)
            throws IOException, ServletException
    {

        final String userKey = getUserKey(request);
        request.getRequestDispatcher(request.getContextPath() + "/secure/UserProfileAction.jspa?userKey=" + userKey).forward(request, response);
    }

    private String getUserKey(HttpServletRequest request)
    {
        String userKey = request.getParameter("name");
        if (StringUtils.isBlank(userKey))
        {
            ApplicationUser user = userManager.getUserByName(request.getRemoteUser());

            if (user != null)
            {
                userKey = user.getKey();
            }
        }

        return userKey;
    }
}
