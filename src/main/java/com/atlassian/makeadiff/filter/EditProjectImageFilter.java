package com.atlassian.makeadiff.filter;

import com.atlassian.core.filters.AbstractHttpFilter;
import com.atlassian.jira.util.JiraKeyUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Filter for editing the project image.
 */
public class EditProjectImageFilter extends AbstractHttpFilter
{
    @Override
    protected void doFilter(final HttpServletRequest request, final HttpServletResponse response, final FilterChain filterChain)
            throws IOException, ServletException
    {
        final String projectKey = getProjectKey(request);
        if (projectKey == null)
        {
            filterChain.doFilter(request, response);
        }
        else
        {
            request.getRequestDispatcher(request.getContextPath() + "/secure/EditProjectImage!default.jspa?key=" + projectKey).forward(request, response);
        }
    }

    private String getProjectKey(final HttpServletRequest request)
    {
        final String pathInfo = getPathInfo(request);
        if (pathInfo != null && !pathInfo.isEmpty())
        {
            if (!JiraKeyUtils.validIssueKey(pathInfo))
            {
                if (!pathInfo.contains("/"))
                {
                    return pathInfo;
                }
            }
        }

        return null;
    }

    private static String getPathInfo(final HttpServletRequest request)
    {
        final String pathInfo = request.getRequestURI();
        if (pathInfo == null)
        {
            return null;
        }

        return pathInfo.substring(pathInfo.lastIndexOf('/') + 1);
    }
}
