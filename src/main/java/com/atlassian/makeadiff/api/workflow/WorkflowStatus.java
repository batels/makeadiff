package com.atlassian.makeadiff.api.workflow;

/**
 * Contains the information required to create a workflow status.
 */
public class WorkflowStatus
{
    private final String name;
    private final String description;
    private final String iconUrl;

    public WorkflowStatus(final String name, final String description, final String iconUrl)
    {
        this.name = name;
        this.description = description;
        this.iconUrl = iconUrl;
    }

    public String getName()
    {
        return name;
    }

    public String getDescription()
    {
        return description;
    }

    public String getIconUrl()
    {
        return iconUrl;
    }
}