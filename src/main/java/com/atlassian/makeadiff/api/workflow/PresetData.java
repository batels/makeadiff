package com.atlassian.makeadiff.api.workflow;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Holds all information required to create and query for a workflow preset
 * <p/>
 * Copied from com.atlassian.greenhopper.service.workflow.PresetData.
 */
public class PresetData
{
    private final String schemeName;
    private final String schemeDescription;
    private final String workflowName;
    private final String workflowDescription;
    private final List<WorkflowStatus> statuses;
    private final List<String> resolutionNames;
    private final String workflowPath;

    static Builder builder()
    {
        return new Builder();
    }

    static Builder builder(PresetData source)
    {
        return new Builder(source);
    }

    static class Builder
    {
        private String schemeName;
        private String schemeDescription;
        private String workflowName;
        private String workflowDescription;
        private List<WorkflowStatus> statuses;
        private List<String> resolutionNames;
        private String workflowPath;

        Builder()
        {
        }

        Builder(PresetData source)
        {
            this.schemeName = source.schemeName;
            this.schemeDescription = source.schemeDescription;
            this.workflowName = source.workflowName;
            this.workflowDescription = source.workflowDescription;
            this.statuses = new ArrayList<WorkflowStatus>(source.statuses);
            this.resolutionNames = new ArrayList<String>(source.resolutionNames);
            this.workflowPath = source.workflowPath;
        }

        Builder schemeName(String schemeName)
        {
            this.schemeName = schemeName;
            return this;
        }

        Builder schemeDescription(String schemeDescription)
        {
            this.schemeDescription = schemeDescription;
            return this;
        }

        Builder workflowName(String workflowName)
        {
            this.workflowName = workflowName;
            return this;
        }

        Builder workflowDescription(String workflowDescription)
        {
            this.workflowDescription = workflowDescription;
            return this;
        }

        Builder statuses(List<WorkflowStatus> statuses)
        {
            this.statuses = statuses;
            return this;
        }

        Builder resolutionNames(String... resolutionNames)
        {
            this.resolutionNames = Arrays.asList(resolutionNames);
            return this;
        }

        Builder workflowPath(String workflowPath)
        {
            this.workflowPath = workflowPath;
            return this;
        }

        PresetData build()
        {
            return new PresetData(schemeName, schemeDescription, workflowName, workflowDescription, statuses, resolutionNames, workflowPath);
        }
    }

    private PresetData(String schemeName, String schemeDescription, String workflowName, String workflowDescription, List<WorkflowStatus> statuses, List<String> resolutionNames, String workflowPath)
    {
        this.schemeName = schemeName;
        this.schemeDescription = schemeDescription;
        this.workflowName = workflowName;
        this.workflowDescription = workflowDescription;
        this.statuses = statuses;
        this.resolutionNames = resolutionNames;
        this.workflowPath = workflowPath;
    }

    public String getSchemeName()
    {
        return schemeName;
    }

    public String getSchemeDescription()
    {
        return schemeDescription;
    }

    public String getWorkflowName()
    {
        return workflowName;
    }

    public String getWorkflowDescription()
    {
        return workflowDescription;
    }

    public List<WorkflowStatus> getStatuses()
    {
        return statuses;
    }

    public List<String> getResolutionNames()
    {
        return resolutionNames;
    }

    public String getWorkflowPath()
    {
        return workflowPath;
    }
}
