package com.atlassian.makeadiff.api;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectCategory;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import org.ofbiz.core.entity.GenericValue;

import java.util.Collection;
import java.util.List;

/**
 * Entity to represent a "Make a Diff" project.
 */
public class FoundationProject implements Project
{
    public enum Status
    {
        NEW("new", "makeadiff.project.status.new.label"),
        IN_PROGRESS("inprogress", "makeadiff.project.status.inprogress.label"),
        COMPLETED("completed", "makeadiff.project.status.completed.label");

        private final String storageKey;

        private final String i18nKey;

        private Status(String storageKey, String i18nKey)
        {
            this.storageKey = storageKey;
            this.i18nKey = i18nKey;
        }

        public String getStorageKey()
        {
            return storageKey;
        }

        public String getI18nKey()
        {
            return i18nKey;
        }

        public String getLabel(final ApplicationUser user)
        {
            return getI18n(user).getText(i18nKey);
        }

        private static I18nHelper getI18n(ApplicationUser user)
        {
            I18nHelper.BeanFactory beanFactory = ComponentAccessor.getComponent(I18nHelper.BeanFactory.class);
            return beanFactory.getInstance(user);
        }

        public static Status findByStorageKey(final String storageKey)
        {
            for (final Status status : values())
            {
                if (storageKey.equals(status.getStorageKey()))
                {
                    return status;
                }
            }

            return null;
        }
    }

    private final Project project;
    private final FoundationProjectEntity entity;
    private final FoundationProjectService projectService;

    // Cached values
    private ApplicationUser creator;
    private List<FoundationProjectContributor> contributors;
    private Status status;

    /**
     * Do not construct instances directly, use {@link FoundationProjectService#convertToFoundationProjectEntity(com.atlassian.jira.project.Project)}
     */
    FoundationProject(final Project project, final FoundationProjectEntity entity, final FoundationProjectService projectService)
    {
        this.project = project;
        this.entity = entity;
        this.projectService = projectService;
    }

    public List<FoundationProjectContributor> getContributors()
    {
        if (contributors == null)
        {
            contributors = projectService.getContributorsForProject(this);
        }
        return contributors;
    }

    public ApplicationUser getCreator()
    {
        if (creator == null)
        {
            creator = ComponentAccessor.getUserManager().getUserByKey(entity.getCreatorUserKey());
        }

        return creator;
    }

    public Status getStatus()
    {
        if (status == null)
        {
            status = Status.findByStorageKey(entity.getStatusKey());
        }

        return status;
    }

    public boolean isNew()
    {
        return getStatus() == Status.NEW;
    }

    public boolean isInProgress()
    {
        return getStatus() == Status.IN_PROGRESS;
    }

    public boolean isCompleted()
    {
        return getStatus() == Status.COMPLETED;
    }

    public Long getAgileBoardId()
    {
        return entity.getAgileBoardId();
    }

    @Override
    public Long getId()
    {
        return project.getId();
    }

    @Override
    public String getName()
    {
        return project.getName();
    }

    @Override
    public String getKey()
    {
        return project.getKey();
    }

    @Override
    public String getUrl()
    {
        return project.getUrl();
    }


    @Override
    public String getEmail()
    {
        return project.getEmail();
    }

    @Override
    public User getLead()
    {
        return project.getLead();
    }

    @Override
    public User getLeadUser()
    {
        return project.getLeadUser();
    }

    @Override
    public String getLeadUserName()
    {
        return project.getLeadUserName();
    }

    public String getDescription()
    {
        return project.getDescription();
    }

    @Override
    public Long getAssigneeType()
    {
        return project.getAssigneeType();
    }

    @Override
    public Long getCounter()
    {
        return project.getCounter();
    }

    @Override
    public Collection<GenericValue> getComponents()
    {
        return project.getComponents();
    }

    @Override
    public Collection<ProjectComponent> getProjectComponents()
    {
        return project.getProjectComponents();
    }

    @Override
    public Collection<Version> getVersions()
    {
        return project.getVersions();
    }

    @Override
    public Collection<IssueType> getIssueTypes()
    {
        return project.getIssueTypes();
    }

    @Override
    public GenericValue getProjectCategory()
    {
        return project.getProjectCategory();
    }

    @Override
    public ProjectCategory getProjectCategoryObject()
    {
        return project.getProjectCategoryObject();
    }

    @Override
    public GenericValue getGenericValue()
    {
        return project.getGenericValue();
    }

    @Override
    public Avatar getAvatar()
    {
        return project.getAvatar();
    }

    @Override
    public ApplicationUser getProjectLead()
    {
        return project.getProjectLead();
    }

    @Override
    public String getLeadUserKey()
    {
        return project.getLeadUserKey();
    }

    @Override
    public String getOriginalKey()
    {
        return project.getOriginalKey();
    }

    public void setStatus(Status status)
    {
        this.entity.setStatusKey(status.getStorageKey());
    }

    public String getLocation()
    {
        return entity.getLocation();
    }

    public void setLocation(String location)
    {
        entity.setLocation(location);
    }

    public String getRepo()
    {
        return entity.getRepo();
    }

    public void setRepo(String repo)
    {
        entity.setRepo(repo);
    }

}
