package com.atlassian.makeadiff.api;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * A representation of a {@link FoundationProject} at the basic storage level.
 */
@XmlRootElement
public class FoundationProjectEntity
{
    @XmlElement
    private String key;

    @XmlElement
    private String creatorUserKey;

    @XmlElement
    private String statusKey;

    @XmlElement
    private String location;

    @XmlElement
    private String repo;

    @XmlElement
    private Long agileBoardId;

    public String getKey()
    {
        return key;
    }

    public void setKey(String key)
    {
        this.key = key;
    }

    public String getCreatorUserKey()
    {
        return creatorUserKey;
    }

    public void setCreatorUserKey(String creatorUserKey)
    {
        this.creatorUserKey = creatorUserKey;
    }

    public String getStatusKey()
    {
        return statusKey;
    }

    public void setStatusKey(String statusKey)
    {
        this.statusKey = statusKey;
    }

    public void setLocation(String location)
    {
        this.location = location;
    }

    public String getLocation()
    {
        return location;
    }

    public void setRepo(String repo)
    {
        this.repo = repo;
    }

    public String getRepo()
    {
        return repo;
    }

    public Long getAgileBoardId()
    {
        return agileBoardId;
    }

    public void setAgileBoardId(Long agileBoardId)
    {
        this.agileBoardId = agileBoardId;
    }
}
