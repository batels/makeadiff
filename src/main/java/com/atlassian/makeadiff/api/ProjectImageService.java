package com.atlassian.makeadiff.api;

import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.ServiceOutcomeImpl;
import com.atlassian.jira.config.util.JiraHome;
import org.apache.commons.lang3.StringUtils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.UUID;

public class ProjectImageService
{
    private final ImageUploadService imageUploadService;
    private final JiraHome jiraHome;
    private final ImageScaler imageScaler;

    public ProjectImageService(final ImageUploadService imageUploadService, final JiraHome jiraHome, final ImageScaler imageScaler)
    {
        this.imageUploadService = imageUploadService;
        this.jiraHome = jiraHome;
        this.imageScaler = imageScaler;
    }

    public boolean hasBannerImage(final String projectKey)
    {
        final File imageFile = getBannerImage(projectKey, ProjectImageSize.FULL);
        if (imageFile == null || !imageFile.exists())
        {
            return false;
        }
        return true;
    }

    /**
     * Gets the banner image for the given project.
     *
     * @param projectKey
     * @return the File, or null if no banner image is found
     */
    public File getBannerImage(final String projectKey, final ProjectImageSize size)
    {
        if (StringUtils.isBlank(projectKey))
        {
            return null;
        }

        File imageFile = getProjectImageFile(projectKey, size);
        if (!imageFile.exists())
        {
            // Try to create the file now, this will only work if the size is not FULL
            createScaledImageOnTheFly(projectKey, size);
            imageFile = getProjectImageFile(projectKey, size);
            if (!imageFile.exists())
            {
                return null;
            }
        }

        return imageFile;
    }

    public ServiceOutcome<String> uploadTempImage(final ImageDescriptor imageDescriptor)
    {
        final String tempFileId = UUID.randomUUID().toString();
        final File imageFile = new File(getTempImageDirectory(), addExtension(tempFileId));
        final ServiceOutcome<Void> result = imageUploadService.upload(imageDescriptor, imageFile);
        return new ServiceOutcomeImpl<String>(result.getErrorCollection(), tempFileId);
    }

    public File getTempImage(final String tempFileId)
    {
        final File file = new File(getTempImageDirectory(), addExtension(tempFileId));
        return (file.exists()) ? file : null;
    }

    public boolean saveProjectImage(final String projectKey, final String tempFileId) throws IOException
    {
        final File imageFile = new File(getTempImageDirectory(), addExtension(tempFileId));
        if (!imageFile.exists())
        {
            return false;
        }

        final File destinationFile = getProjectImageFile(projectKey, ProjectImageSize.FULL);
        boolean success = imageFile.renameTo(destinationFile);
        if (success)
        {
            createScaledImages(destinationFile, projectKey);
        }
        return success;
    }

    private void createScaledImages(final File imageFile, final String projectKey) throws IOException
    {
        final BufferedImage image = ImageIO.read(imageFile);
        createScaledImage(image, projectKey, ProjectImageSize.BIG);
        createScaledImage(image, projectKey, ProjectImageSize.SMALL);
    }

    private void createScaledImage(final BufferedImage image, final String projectKey, final ProjectImageSize size) throws IOException
    {
        final File destinationFile = getProjectImageFile(projectKey, size);
        final BufferedImage scaledImage = imageScaler.scaleImage(image, size.getWidth(), size.getHeight());
        ImageIO.write(scaledImage, ImageUploadService.IMAGE_OUTPUT_FORMAT, destinationFile);
    }

    private void createScaledImageOnTheFly(final String projectKey, final ProjectImageSize size)
    {
        if (size == null || size == ProjectImageSize.FULL)
        {
            return;
        }

        final File fullImage = getProjectImageFile(projectKey, ProjectImageSize.FULL);
        if (!fullImage.exists())
        {
            // No original image to scale from
            return;
        }

        try
        {
            final BufferedImage image = ImageIO.read(fullImage);
            createScaledImage(image, projectKey, size);
        }
        catch (IOException e)
        {
            // Do nothing, it was a long shot anyway
        }
    }

    private File getTempImageDirectory()
    {
        return getDirectory(jiraHome.getHomePath() + "/images/temp");
    }

    private File getProjectImageDirectory(final String projectKey)
    {
        return getDirectory(jiraHome.getHomePath() + "/images/project/" + projectKey);
    }

    private File getProjectImageFile(final String projectKey, final ProjectImageSize size)
    {
        final String filename;
        switch (size)
        {
            case BIG:
            {
                filename = addExtension("banner_big");
                break;
            }
            case SMALL:
            {
                filename = addExtension("banner_small");
                break;
            }
            case FULL:
            default:
            {
                filename = addExtension("banner");
            }
        }
        return new File(getProjectImageDirectory(projectKey), filename);
    }

    private File getDirectory(final String path)
    {
        final File directory = new File(path);
        if (!directory.exists())
        {
            directory.mkdirs();
        }

        return directory;
    }

    private String addExtension(final String s)
    {
        return s + "." + ImageUploadService.IMAGE_OUTPUT_FORMAT;
    }
}
