package com.atlassian.makeadiff.api;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 23/03/13
 * Time: 4:17 PM
 * To change this template use File | Settings | File Templates.
 */
public class TaskCreationProperties
{
    Long projectId;
    String issueTypeId;
    String summary;
    String reporterId;
    String assigneeId;
    String description;
    String environment;
    String statusId;
    String priorityId;
    String resolutionId;
    Long securityLevelId;
    Long[] fixVersionId;

    public Long getProjectId()
    {
        return projectId;
    }

    public void setProjectId(Long projectId)
    {
        this.projectId = projectId;
    }

    public String getIssueTypeId()
    {
        return issueTypeId;
    }

    public void setIssueTypeId(String issueTypeId)
    {
        this.issueTypeId = issueTypeId;
    }

    public String getSummary()
    {
        return summary;
    }

    public void setSummary(String summary)
    {
        this.summary = summary;
    }

    public String getReporterId()
    {
        return reporterId;
    }

    public void setReporterId(String reporterId)
    {
        this.reporterId = reporterId;
    }

    public String getAssigneeId()
    {
        return assigneeId;
    }

    public void setAssigneeId(String assigneeId)
    {
        this.assigneeId = assigneeId;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getEnvironment()
    {
        return environment;
    }

    public void setEnvironment(String environment)
    {
        this.environment = environment;
    }

    public String getStatusId()
    {
        return statusId;
    }

    public void setStatusId(String statusId)
    {
        this.statusId = statusId;
    }

    public String getPriorityId()
    {
        return priorityId;
    }

    public void setPriorityId(String priorityId)
    {
        this.priorityId = priorityId;
    }

    public String getResolutionId()
    {
        return resolutionId;
    }

    public void setResolutionId(String resolutionId)
    {
        this.resolutionId = resolutionId;
    }

    public Long getSecurityLevelId()
    {
        return securityLevelId;
    }

    public void setSecurityLevelId(Long securityLevelId)
    {
        this.securityLevelId = securityLevelId;
    }

    public void setFixVersionId(Long... fixVersionId)
    {
        this.fixVersionId = fixVersionId;
    }

}
