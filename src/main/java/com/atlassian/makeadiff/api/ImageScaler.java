package com.atlassian.makeadiff.api;

import com.atlassian.core.util.thumbnail.Thumber;
import com.atlassian.core.util.thumbnail.Thumbnail;

import java.awt.image.BufferedImage;

/**
 * Scales images.
 */
public class ImageScaler
{
    private final Thumber thumber = new Thumber(Thumbnail.MimeType.PNG);

    /**
     * @param image  the image to scale
     * @param width  the scaled width, if 0 this is ignored
     * @param height the scaled height, if 0 this is ignored
     * @return a scaled image
     */
    public BufferedImage scaleImage(final BufferedImage image, final int width, final int height)
    {
        if (width != 0 && height != 0)
        {
            return scaleAndCrop(image, width, height);
        }
        else if (width != 0)
        {
            return scaleImageToWidth(image, width);
        }
        else if (height != 0)
        {
            return scaleImageToHeight(image, height);
        }

        // No scaling required
        return image;
    }

    /**
     * @param image        the image to scale
     * @param scaledHeight the height of the scaled image
     * @return if the image height was bigger than the height given, scaled image is return with height equal to the given height, otherwise the the image is returned unchanged
     */
    public BufferedImage scaleImageToHeight(final BufferedImage image, final int scaledHeight)
    {
        final int imageHeight = image.getHeight(null);
        if (imageHeight <= scaledHeight)
        {
            return image;
        }
        final int imageWidth = image.getWidth(null);
        final double ratio = (double) imageHeight / scaledHeight;
        final int scaledWidth = (int) (imageWidth / ratio);

        final Thumber.WidthHeightHelper dimensionHelper = new Thumber.WidthHeightHelper(scaledWidth, scaledHeight);
        return thumber.scaleImage(image, dimensionHelper);
    }

    /**
     * @param image       the image to scale
     * @param scaledWidth the width of the scaled image
     * @return if the image width was bigger than the width given, scaled image is return with width equal to the given width, otherwise the the image is returned unchanged
     */
    public BufferedImage scaleImageToWidth(final BufferedImage image, final int scaledWidth)
    {
        final int imageWidth = image.getWidth(null);
        if (imageWidth <= scaledWidth)
        {
            return image;
        }

        final int imageHeight = image.getHeight(null);
        final double ratio = (double) imageWidth / scaledWidth;
        final int scaledHeight = (int) (imageHeight / ratio);

        final Thumber.WidthHeightHelper dimensionHelper = new Thumber.WidthHeightHelper(scaledWidth, scaledHeight);
        return thumber.scaleImage(image, dimensionHelper);
    }

    private BufferedImage scaleAndCrop(final BufferedImage image, final int width, final int height)
    {
        final int imageWidth = image.getWidth(null);
        final int imageHeight = image.getHeight(null);
        if (imageWidth <= width && imageHeight <= height)
        {
            return image;
        }

        double imageRatio = (double) imageWidth / (double) imageHeight;
        double scaledRatio = (double) width / (double) height;

        final BufferedImage scaledImage;
        if (scaledRatio < imageRatio)
        {
            // Scaled ratio is taller than the original ratio
            scaledImage = scaleImageToHeight(image, height);
        }
        else
        {
            // Scaled ratio is wider than the original ratio
            scaledImage = scaleImageToWidth(image, width);
        }

        return cropImage(scaledImage, width, height);
    }

    private BufferedImage cropImage(final BufferedImage image, int cropWidth, int cropHeight)
    {
        final int imageWidth = image.getWidth(null);
        final int imageHeight = image.getHeight(null);

        // Make sure we don't try to crop larger than the original image
        cropWidth = Math.min(imageWidth, cropWidth);
        cropHeight = Math.min(imageHeight, cropHeight);

        int xOffset = (imageWidth - cropWidth) / 2;
        int yOffset = (imageHeight - cropHeight) / 2;
        return image.getSubimage(xOffset, yOffset, cropWidth, cropHeight);
    }
}
