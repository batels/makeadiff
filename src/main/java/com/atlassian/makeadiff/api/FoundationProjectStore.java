package com.atlassian.makeadiff.api;

import java.util.Collection;

/**
 * Data access layer for {@link FoundationProject}s.
 */
public interface FoundationProjectStore
{
    FoundationProjectEntity getProject(String projectKey);

    Collection<FoundationProjectEntity> getAllProjects();

    void addProject(FoundationProjectEntity project);

    void updateProject(FoundationProjectEntity project);
}
