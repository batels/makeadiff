package com.atlassian.makeadiff.api;

import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.ServiceOutcomeImpl;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Set;


public class ImageUploadService
{
    public static final String IMAGE_OUTPUT_FORMAT = "png";

    private static final Set<String> CONTENT_TYPES = new HashSet<String>(Lists.newArrayList(
            "image/jpeg", "image/gif", "image/png", "image/pjpeg", "image/x-png"
    ));

    private final JiraAuthenticationContext authenticationContext;

    public ImageUploadService(final JiraAuthenticationContext authenticationContext)
    {
        this.authenticationContext = authenticationContext;
    }

    public ServiceOutcome<Void> upload(final ImageDescriptor imageDescriptor, final File destinationFile)
    {
        final ErrorCollection errors = new SimpleErrorCollection();

        validate(imageDescriptor, errors);
        if (!errors.hasAnyErrors())
        {
            saveImage(imageDescriptor.getInputStream(), destinationFile, errors);
        }

        return new ServiceOutcomeImpl<Void>(errors);
    }

    private void validate(final ImageDescriptor imageDescriptor, final ErrorCollection errors)
    {
        if (StringUtils.isNotEmpty(imageDescriptor.getFilename()))
        {
            if (!isContentTypeSupported(imageDescriptor.getContentType()))
            {
                errors.addErrorMessage(getI18nHelper().getText("makeadiff.project.image.upload.error.mimetype.unsupported", imageDescriptor.getContentType()));
            }
            else if (imageDescriptor.getInputStream() == null)
            {
                errors.addErrorMessage(getI18nHelper().getText("makeadiff.project.image.upload.error"));
            }
        }
        else
        {
            errors.addErrorMessage(getI18nHelper().getText("makeadiff.project.image.upload.error"));
        }
    }

    private void saveImage(final InputStream inputStream, final File destinationFile, final ErrorCollection errors)
    {
        try
        {
            saveLogoImageData(inputStream, destinationFile);
        }
        catch (IOException e)
        {
            errors.addErrorMessage(getI18nHelper().getText("makeadiff.project.image.upload.error") + e.getMessage());
        }
    }

    private void saveLogoImageData(final InputStream inputStream, final File destinationFile) throws IOException
    {
        final BufferedImage image = ImageIO.read(inputStream);
        writeImageFile(image, IMAGE_OUTPUT_FORMAT, destinationFile);
    }

    private void writeImageFile(final BufferedImage image, final String output, final File destinationFile) throws IOException
    {
        ImageIO.write(image, output, destinationFile);
    }

    private boolean isContentTypeSupported(final String contentType)
    {
        return StringUtils.isNotBlank(contentType) && CONTENT_TYPES.contains(contentType.toLowerCase());
    }

    private I18nHelper getI18nHelper()
    {
        return authenticationContext.getI18nHelper();
    }
}
