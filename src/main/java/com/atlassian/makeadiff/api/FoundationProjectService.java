package com.atlassian.makeadiff.api;

import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.ServiceResultImpl;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.bc.projectroles.ProjectRoleService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.fields.screen.issuetype.IssueTypeScreenSchemeManager;
import com.atlassian.jira.issue.label.Label;
import com.atlassian.jira.notification.NotificationSchemeManager;
import com.atlassian.jira.permission.PermissionSchemeManager;
import com.atlassian.jira.project.AssigneeTypes;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.security.roles.ProjectRoleActor;
import com.atlassian.jira.security.roles.ProjectRoleActors;
import com.atlassian.jira.security.roles.ProjectRoleImpl;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.security.roles.RoleActor;
import com.atlassian.jira.security.roles.RoleActorDoesNotExistException;
import com.atlassian.jira.security.roles.RoleActorFactory;
import com.atlassian.jira.security.roles.actor.UserRoleActorFactory;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.workflow.WorkflowSchemeManager;
import com.atlassian.makeadiff.api.workflow.SimplifiedWorkflowService;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.opensymphony.util.TextUtils;
import org.apache.commons.lang.StringUtils;
import org.ofbiz.core.entity.GenericEntityException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.util.*;

/**
 * Service to get, edit and create "Make a Diff" projects.
 */
public class FoundationProjectService
{
    private static final Logger log = LoggerFactory.getLogger(FoundationProjectService.class);
    public static final String CHAMPION_ROLE = "champion";

    private final FoundationProjectStore store;
    private final ProjectManager projectManager;
    private final ProjectService projectService;
    private final IssueManager issueManager;
    private final WorkflowSchemeManager workflowSchemeManager;
    private final NotificationSchemeManager notificationSchemeManager;
    private final IssueTypeScreenSchemeManager issueTypeScreenSchemeManager;
    private final GenerateProjectKeyService generateProjectKeyService;
    private final AgileBoardService agileBoardService;
    private final PermissionSchemeManager permissionSchemeManager;
    private final TaskService taskService;
    private final UserManager userManager;
    private final UserProfileService userProfileService;
    private final SimplifiedWorkflowService simplifiedWorkflowService;
    private final ProjectRoleManager projectRoleManager;
    private final RoleActorFactory roleActorFactory;

    public FoundationProjectService(final FoundationProjectStore store,
        final ProjectManager projectManager,
        final ProjectService projectService,
        final IssueManager issueManager,
        final WorkflowSchemeManager workflowSchemeManager,
        final NotificationSchemeManager notificationSchemeManager,
        final IssueTypeScreenSchemeManager issueTypeScreenSchemeManager,
        final GenerateProjectKeyService generateProjectKeyService,
        final AgileBoardService agileBoardService,
        final PermissionSchemeManager permissionSchemeManager,
        final TaskService taskService,
        final UserManager userManager,
        final UserProfileService userProfileService,
        final SimplifiedWorkflowService simplifiedWorkflowService,
        final ProjectRoleManager projectRoleManager,
        final RoleActorFactory roleActorFactory)
    {
        this.issueManager = issueManager;
        this.store = store;
        this.projectManager = projectManager;
        this.projectService = projectService;
        this.workflowSchemeManager = workflowSchemeManager;
        this.notificationSchemeManager = notificationSchemeManager;
        this.issueTypeScreenSchemeManager = issueTypeScreenSchemeManager;
        this.generateProjectKeyService = generateProjectKeyService;
        this.agileBoardService = agileBoardService;
        this.permissionSchemeManager = permissionSchemeManager;
        this.taskService = taskService;
        this.userManager = userManager;
        this.projectRoleManager = projectRoleManager;
        this.userProfileService = userProfileService;
        this.simplifiedWorkflowService = simplifiedWorkflowService;
        this.roleActorFactory = roleActorFactory;
    }

    private FoundationProject convertToFoundationProjectEntity(Project project)
    {
        // TODO use this when makeadiff project creation is working, for now just mock the project
        //final FoundationProjectEntity entity = store.getProject(projectKey);

        final FoundationProjectEntity entity = store.getProject(project.getKey());
        if (entity == null)
        {
            throw new UnsupportedOperationException("The JIRA project is not a 'Make a Diff' project");
        }

        return new FoundationProject(project, entity, this);
    }

    public FoundationProject getProject(final String projectKey)
    {
        final Project project = projectManager.getProjectObjByKey(projectKey);
        if (project == null)
        {
            return null;
        }
        return convertToFoundationProjectEntity(project);
    }

    private ProjectRole getChampionRole()
    {
        ProjectRole championRole = projectRoleManager.getProjectRole(CHAMPION_ROLE);

        if (championRole == null)
        {
            championRole = projectRoleManager.createRole(new ProjectRoleImpl(CHAMPION_ROLE, "Project Champions"));
        }

        return championRole;
    }

    /**
     * Get the champions for this project
     * @param project
     * @return a ProjectRoleActors object of the Champions, or null if there were errors.
     */
    public ProjectRoleActors getChampions(Project project)
    {
        ProjectRole championRole = getChampionRole();

        return projectRoleManager.getProjectRoleActors(championRole, project); //TODO: does this ever return null?
    }

    public boolean isUserChampion(ApplicationUser user, Project project)
    {
        ProjectRoleActors champions = getChampions(project);
        return champions.contains(user);
    }

    public List<FoundationProjectContributor> getContributorsForProject(FoundationProject project)
    {
        // Examine all resolved tasks in the project and count the number of resolved tasks, grouped by Assignee.
        Map<String, Integer> contributions = Maps.newHashMap();
        List<Issue> resolvedTasks = taskService.getResolvedTasksForProject(project);
        for (Issue task : resolvedTasks)
        {
            if (!contributions.containsKey(task.getAssigneeId()))
            {
                contributions.put(task.getAssigneeId(), new Integer(1));
            }
            else
            {
                Integer count = contributions.get(task.getAssigneeId()) + 1;
                contributions.put(task.getAssigneeId(), count);
            }
        }

        // Ensure that the project owner and the project champion (if the role is filled) are
        // both counted as contributors on the project.
        ApplicationUser owner = project.getCreator();
        if (!contributions.containsKey(owner.getKey()))
        {
            contributions.put(owner.getKey(), new Integer(0));
        }

        ProjectRoleActors champions = getChampions(project);
        for (ApplicationUser champion : champions.getApplicationUsers())
        {
            if (!contributions.containsKey(champion.getKey()))
                contributions.put(champion.getKey(), new Integer(0));
        }

        // Create a FoundationProjectContributor for each contributor
        List<FoundationProjectContributor> contributors = new ArrayList<FoundationProjectContributor>(contributions.size());
        for (String userKey : contributions.keySet())
        {
            ApplicationUser user = userManager.getUserByKey(userKey);
            if (user == null)
            {
                // User doesn't exist. WTF?
                log.error(String.format("Unable to load user with key %s; this user's contributions will not be shown for Project %s", userKey, project.getKey()));
                continue;
            }
            UserProfile userProfile = userProfileService.getUserProfile(user.getKey());
            if (userProfile == null)
            {
                // This is unlikely
                log.error(String.format("Unable to load user profile for user %s; this user's contributions will not be shown for Project %s", userKey, project.getKey()));
                continue;
            }
            URI avatarURL = userProfileService.getAvatarURL(userProfile, Avatar.Size.LARGE);
            FoundationProjectContributor.ContributorType type;
            if (user.equals(owner))
            {
                type = FoundationProjectContributor.ContributorType.OWNER;
            }
            else if (champions != null && champions.contains(user))
            {
                type = FoundationProjectContributor.ContributorType.CHAMPION;
            }
            else
            {
                type = FoundationProjectContributor.ContributorType.CONTRIBUTOR;
            }

            contributors.add(new FoundationProjectContributor(project, user, type, userProfile, contributions.get(userKey).intValue(), avatarURL));
        }

        // Sort the collection so that the biggest contributors are listed first. The Project Owner and the Project Champion
        // are automatically regarded as the biggest contributors.
        Collections.sort(contributors, new Comparator<FoundationProjectContributor>()
        {
            @Override
            public int compare(FoundationProjectContributor first, FoundationProjectContributor second)
            {
                if (first.getContributorType().getOrder() == second.getContributorType().getOrder())
                {
                    return new Integer(second.getContributions()).compareTo(new Integer(first.getContributions()));
                }
                else
                {
                    return new Integer(first.getContributorType().getOrder()).compareTo(new Integer(second.getContributorType().getOrder()));
                }
            }
        });
        return contributors;
    }

    public List<FoundationProject> getAllProjects(final ApplicationUser user)
    {
        final ServiceOutcome<List<Project>> allProjects = projectService.getAllProjects(user);

        if (allProjects.isValid())
        {
            return Lists.transform(allProjects.getReturnedValue(), new Function<Project, FoundationProject>()
            {
                @Override
                public FoundationProject apply(final Project input)
                {
                    return convertToFoundationProjectEntity(input);
                }
            });
        }

        return Collections.emptyList();
    }

    public List<FoundationProject> getFeaturedProjects(final ApplicationUser user, final int numberProjects)
    {

        // TODO getting all projects can be really expensive. This needs to be improved

        List<FoundationProject> allProjects = getAllProjects(user);
        Collections.sort(allProjects, new FeaturedProjectComparator());

        List<FoundationProject> featuredProjects = new ArrayList<FoundationProject>();
        int l = allProjects.size();
        if (l > 3) l = 3;
        for (int i = 0; i < l; i++)
        {
            featuredProjects.add(allProjects.get(i));
        }
        return featuredProjects;
    }

    public List<Label> getProjectLabels(Project project) throws GenericEntityException
    {
        List<Label> labels = new ArrayList<Label>();
        for (Long issueID : issueManager.getIssueIdsForProject(project.getId()))
        {
            for (Label l : issueManager.getIssueObject(issueID).getLabels())
            {
                labels.add(l);
            }

        }
        return labels;
    }

    /**
     * This method returns all the labels of all the issues of all the projects.
     * It is an impractical query and is not suitable for production, but is included
     * as a proof of concept for the Skills list on the home page.
     *
     * @return
     * @throws GenericEntityException
     */
    public List<Label> getAllLabels() throws GenericEntityException
    {
        List<Label> labels = new ArrayList<Label>();
        for (Project p : projectManager.getProjectObjects())
        {
            labels.addAll(getProjectLabels(p));
        }
        return labels;
    }

    public Map<String, Long> getAllLabelsWeighted() throws GenericEntityException
    {
        List<Label> labels = getAllLabels();
        Map<String, Long> weightedLabels = new HashMap<String, Long>();
        for (Label l : labels)
        {
            String stringLabel = l.getLabel().trim();
            Long aLong = weightedLabels.get(stringLabel);
            weightedLabels.put(stringLabel, aLong == null ? 1 : aLong + 1);
        }
        return weightedLabels;
    }


    public ValidationResult validateCreateProject(final String name, final String description, final FoundationProjectEntity entity, String url, String location, String repo)
    {
        final ErrorCollection errorCollection = new SimpleErrorCollection();

        // Make sure they entered a non-empty name (or a name which is just whitespace).
        if (StringUtils.isBlank(name))
        {
            errorCollection.addError("name", "The name must contain at least one character.", ErrorCollection.Reason.VALIDATION_FAILED);
        }

        if (StringUtils.isNotBlank(url) && !TextUtils.verifyUrl(url))
        {
            errorCollection.addError("url", "URL needs to be a valid URL", ErrorCollection.Reason.VALIDATION_FAILED);
        }

        // TODO check permissions
        // TODO validate request
        // TODO allow user to immediately set avatar?

        String key;
        try
        {
            key = generateProjectKeyService.generateUniqueKey(name);
        }
        catch (IllegalArgumentException e)
        {
            errorCollection.addError("name", e.getMessage());
            key = null;
        }

        return new ValidationResult(errorCollection,
                name,
                description,
                url,
                entity,
                key,
                location,
                repo);
    }

    public void createProject(final ValidationResult validationResult)
    {
        final FoundationProjectEntity entity = validationResult.getEntity();
        entity.setKey(validationResult.getKey());

        final Project project = projectManager.createProject(
                validationResult.getName(), entity.getKey(), validationResult.getDescription(),
                entity.getCreatorUserKey(), validationResult.getUrl(),
                AssigneeTypes.UNASSIGNED);

        // Mimic the things that the jira ProjectService would do
        associateWithDefaultSchemes(project);
        workflowSchemeManager.clearWorkflowCache();

        simplifiedWorkflowService.associateDefaultSchemeToProject(project);

        // Ensure the status is set to NEW for new projects
        entity.setStatusKey(FoundationProject.Status.NEW.getStorageKey());

        // Create an agile board for this project
        entity.setAgileBoardId(agileBoardService.createAgileBoard(project));

        store.addProject(entity);
    }

    private void associateWithDefaultSchemes(Project newProject)
    {
        issueTypeScreenSchemeManager.associateWithDefaultScheme(newProject);
        notificationSchemeManager.addDefaultSchemeToProject(newProject);
        permissionSchemeManager.addDefaultSchemeToProject(newProject);
    }

    public void updateProject(final FoundationProject project, final ValidationResult validationResult)
    {
        final FoundationProjectEntity entity = validationResult.getEntity();

        // TODO: Fix project lead and project URL
        projectManager.updateProject(
            project, validationResult.getName(), validationResult.getDescription(),
            project.getProjectLead().getKey(), validationResult.getUrl(),
            AssigneeTypes.UNASSIGNED);
        store.updateProject(entity);
        project.setRepo(validationResult.getRepo());


    }

    public boolean isAllowedToEdit(final ApplicationUser user, final FoundationProject project)
    {
        if (user == null)
        {
            return false;
        }

        return isUserEqual(user, project.getCreator())
                || isUserEqual(user, project.getProjectLead())
                || isUserChampion(user, project);
    }

    private boolean isUserEqual(final ApplicationUser user1, final ApplicationUser user2)
    {
        // If either are null, they are not equal - we don't consider both being null to be equal
        if (user1 == null || user2 == null)
        {
            return false;
        }

        return user1.getKey().equals(user2.getKey());
    }

    /**
     * Add a champion
     * @param projectKey
     * @param user
     * @return true if successful, false if not.
     */
    public boolean addChampion(String projectKey, ApplicationUser user)
    {
        ProjectRole championRole = getChampionRole();

        final Project project = projectManager.getProjectObjByKey(projectKey);
        ProjectRoleActors champions = getChampions(project);
        if (!champions.contains(user))
        {
            try
            {
                ProjectRoleActor roleActor = roleActorFactory.createRoleActor(null, championRole.getId(),
                    project.getId(), UserRoleActorFactory.TYPE, user.getKey());
                champions = (ProjectRoleActors) champions.addRoleActors(Arrays.asList((RoleActor) roleActor));
                projectRoleManager.updateProjectRoleActors(champions);
                return true;
            }
            catch (RoleActorDoesNotExistException e)
            {
                //This should never happen
            }
        }

        return false;
    }

    /**
     * Returned when creating a {@link FoundationProject}.
     */
    public static class ValidationResult extends ServiceResultImpl
    {
        private final String name;
        private final String description;
        private final String url;
        private final FoundationProjectEntity entity;
        private final String key;
        private final String location;
        private final String repo;

        public ValidationResult(final ErrorCollection errorCollection,
                                final String name,
                                final String description,
                                final String url,
                                final FoundationProjectEntity entity,
                                final String key,
                                final String location,
                                final String repo)
        {
            super(errorCollection);
            this.name = name;
            this.description = description;
            this.url = url;
            this.entity = entity;
            this.key = key;
            this.location = location;
            this.repo = repo;
        }

        public String getName()
        {
            return name;
        }

        public String getDescription()
        {
            return description;
        }

        public String getUrl()
        {
            return url;
        }

        public String getRepo()
        {
            return repo;
        }

        public FoundationProjectEntity getEntity()
        {
            return entity;
        }

        public String getKey()
        {
            return key;
        }

        public String getLocation()
        {
            return location;
        }
    }
}
