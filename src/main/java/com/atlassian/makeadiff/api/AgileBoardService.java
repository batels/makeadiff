package com.atlassian.makeadiff.api;

import com.atlassian.greenhopper.api.rapid.view.RapidViewCreationService;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.search.SearchRequestManager;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.sharing.SharePermission;
import com.atlassian.jira.sharing.SharePermissionImpl;
import com.atlassian.jira.sharing.SharedEntity;
import com.atlassian.jira.sharing.type.ProjectShareType;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.query.Query;
import com.atlassian.query.order.SortOrder;

import java.util.HashSet;
import java.util.Set;

/**
 * Service for creating JIRA Agile boards for projects.
 */
public class AgileBoardService
{
    private final RapidViewCreationService rapidViewCreationService;
    private final SearchRequestManager searchRequestManager;

    public AgileBoardService(final RapidViewCreationService rapidViewCreationService,
                             final SearchRequestManager searchRequestManager)
    {
        this.rapidViewCreationService = rapidViewCreationService;
        this.searchRequestManager = searchRequestManager;
    }

    public Long createAgileBoard(Project project)
    {
        final ErrorCollection errors = new SimpleErrorCollection();
        final SearchRequest savedFilter = createSavedFilter(project.getProjectLead(), project);
        final Long rapidViewId = rapidViewCreationService.createNewRapidView(project.getLead(), savedFilter.getName(), savedFilter.getId(), errors);

        if (errors.hasAnyErrors())
        {
            // TODO log errors
        }

        return rapidViewId;
    }

    public SearchRequest createSavedFilter(ApplicationUser user, Project project)
    {
        final Query query = buildQuery(project);
        final SharedEntity.SharePermissions permissions = buildPermissions(project);

        final String filterName = findAvailableFilterName(user, project);
        final SearchRequest savedFilter = new SearchRequest(query, user.getName(), filterName, null);
        savedFilter.setPermissions(permissions);

        return searchRequestManager.create(savedFilter);
    }

    private Query buildQuery(Project project)
    {
        JqlQueryBuilder queryBuilder = JqlQueryBuilder.newBuilder();
        queryBuilder.where().project(project.getKey());
        queryBuilder.orderBy().addSortForFieldName("Rank", SortOrder.ASC, true);
        return queryBuilder.buildQuery();
    }

    private SharedEntity.SharePermissions buildPermissions(Project project)
    {
        Set<SharePermission> shares = new HashSet<SharePermission>();
        shares.add(new SharePermissionImpl(ProjectShareType.TYPE, String.valueOf(project.getId()), null));
        return new SharedEntity.SharePermissions(shares);
    }

    private String findAvailableFilterName(ApplicationUser user, Project project)
    {
        final String baseName = project.getName() + " (" + project.getKey() + ") Tasks";
        int i = 1;
        do
        {
            // The name is not really important, as it should never be seen by the user
            final String newFilterName = (i > 1) ? baseName + " " + i : baseName;
            final SearchRequest existingFilter = searchRequestManager.getOwnedSearchRequestByName(user, newFilterName);
            if (existingFilter == null)
            {
                return newFilterName;
            }
            i++;
        } while (true);
    }
}
