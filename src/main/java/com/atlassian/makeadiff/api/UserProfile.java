package com.atlassian.makeadiff.api;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.user.ApplicationUser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Entity to represent a user's profile.
 */
public class UserProfile
{
    private final String key;
    private final Collection<Organisation> organisations;
    private List<String> skills;
    private String description;

    public UserProfile(final String key, final Collection<Organisation> organisations, final String skills, final String description)
    {
        this.key = key;
        this.organisations = organisations;
        this.skills = new ArrayList<String>();
        if (skills != null)
        {
            this.skills = Arrays.asList(skills.split(";"));
        }

//        if(skills != null){
//            for(String skill : skills)
//            {
//                if(!this.skills.contains(skill))
//                {
//                    this.skills.add(skill);
//                }
//            }
//        }
        this.description = description;
    }

    public String getKey()
    {
        return key;
    }

    public Collection<Organisation> getOrganisations()
    {
        return organisations;
    }

    public boolean belongsToAnyOrganisations()
    {
        return (organisations != null && !organisations.isEmpty());
    }

    public String getDisplayName()
    {
        ApplicationUser user = ComponentAccessor.getUserManager().getUserByKey(key);
        return user.getDisplayName();
    }

    public String getUsername()
    {
        ApplicationUser user = ComponentAccessor.getUserManager().getUserByKey(key);
        return user.getUsername();
    }

    public String getSkills()
    {
        StringBuilder sb = new StringBuilder();
        for (String skill : this.skills)
        {
            sb.append(skill);
            sb.append(';');
        }
        if (sb.length() != 0)
        {
            sb.deleteCharAt(sb.length() - 1);
        }
        return sb.toString();
    }

    public void setSkills(String skills)
    {
        if (skills != null)
        {
            this.skills = Arrays.asList(skills.split(";"));
        }
//        for(String skill : skills)
//        {
//            if(!this.skills.contains(skill))
//            {
//                this.skills.add(skill);
//            }
//        }
    }

    public String getDescription()
    {
        return this.description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }


}
