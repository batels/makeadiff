package com.atlassian.makeadiff.api;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.exception.OperationNotPermittedException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.exception.embedded.InvalidGroupException;
import com.atlassian.jira.bc.ServiceResultImpl;
import com.atlassian.jira.exception.PermissionException;
import com.atlassian.jira.exception.RemoveException;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.google.common.base.Predicate;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Nullable;
import java.util.Collection;
import java.util.StringTokenizer;

/**
 * Service to get, edit and create organisations.
 */
public class OrganisationService
{
    private static final int DEFAULT_ORGANISATION_RESULTS_LIMIT = 5;

    private final OrganisationStore store;
    private final GroupManager groupManager;
    private final UserUtil userUtil;

    public OrganisationService(final OrganisationStore store, final GroupManager groupManager, final UserUtil userUtil)
    {
        this.store = store;
        this.groupManager = groupManager;
        this.userUtil = userUtil;
    }

    public Organisation createOrUpdate(final String name, final String url)
    {
        final Group group = groupManager.getGroup(name);
        if (group == null)
        {
            try
            {
                groupManager.createGroup(name);
            }
            catch (OperationNotPermittedException e)
            {
                throw new RuntimeException(e);
            }
            catch (InvalidGroupException e)
            {
                throw new RuntimeException(e);
            }
        }
        return store.createOrUpdateOrganisation(name, url, null, false);
    }

    public void addUserToOrganisation(final String userKey, final Organisation organisation)
    {
        final Group group = groupManager.getGroup(organisation.getName());
        final ApplicationUser appUser = userUtil.getUserByKey(userKey);
        if (appUser != null)
        {
            try
            {
                groupManager.addUserToGroup(appUser.getDirectoryUser(), group);
            }
            catch (GroupNotFoundException e)
            {
                throw new RuntimeException(e);
            }
            catch (UserNotFoundException e)
            {
                throw new RuntimeException(e);
            }
            catch (OperationNotPermittedException e)
            {
                throw new RuntimeException(e);
            }
            catch (OperationFailedException e)
            {
                throw new RuntimeException(e);
            }
        }
    }

    public void removeUserFromOrganisation(final String userKey, final Organisation organisation)
    {
        final Group group = groupManager.getGroup(organisation.getName());
        final ApplicationUser appUser = userUtil.getUserByKey(userKey);
        if (appUser != null)
        {
            try
            {
                userUtil.removeUserFromGroup(group, appUser.getDirectoryUser());
            }
            catch (RemoveException e)
            {
                throw new RuntimeException(e);
            }
            catch (PermissionException e)
            {
                throw new RuntimeException(e);
            }
        }
    }

    public Collection<User> getMembersOfOrganisation(final Organisation organisation)
    {
        return groupManager.getUsersInGroup(organisation.getName());
    }

    public Organisation edit(final String name, final String url, final String description, final boolean verified)
    {
        return store.createOrUpdateOrganisation(name, url, description, verified);
    }

    public Organisation getOrganisationById(final int id)
    {
        return store.getOrganisationById(id);
    }

    public Organisation getOrganisationByKey(final String key)
    {
        return store.getOrganisationByKey(key);
    }

    public Organisation getOrganisationByName(final String name)
    {
        return store.getOrganisationByName(name);
    }

    public Collection<Organisation> search(final String query)
    {
        if (StringUtils.isBlank(query))
        {
            return store.getAllOrganisations(DEFAULT_ORGANISATION_RESULTS_LIMIT);
        }

        return store.findOrganisationByLikeName(query);
    }

    public CreateValidationResult validateCreateOrganisation(final String name, final String url, final FoundationProjectEntity entity)
    {
        final ErrorCollection errorCollection = new SimpleErrorCollection();
        // TODO validate request
        if (name.length() == 0)
        {
            errorCollection.addError("Name Invalid", "Must provide a name.");
        }

        if (url.length() == 0)
        {
            errorCollection.addError("Url Invalid", "Must provide a url.");
        }
        // Avatar cannot be set immediately: Set on main charity edit page (for simpler charity creation)

        return new CreateValidationResult(errorCollection, name, url, entity);
    }

    public Organisation createOrganisation(String name, String url)
    {
        return store.createOrUpdateOrganisation(name, url, null, false);
    }

    public void createOrganisation(String name, String url, String description)
    {
        store.createOrUpdateOrganisation(name, url, description, false);
    }

    private static class SearchMatcherPredicate implements Predicate<Organisation>
    {
        private final String query;

        private SearchMatcherPredicate(final String query)
        {
            this.query = query.toLowerCase();
        }

        @Override
        public boolean apply(@Nullable final Organisation organisation)
        {
            final String lowerCaseName = organisation.getName().toLowerCase();

            if (lowerCaseName.startsWith(query))
            {
                return true;
            }

            // Try each word in the name
            final StringTokenizer tokenizer = new StringTokenizer(lowerCaseName);
            tokenizer.nextToken();    // skip the first word that we know didn't match
            while (tokenizer.hasMoreElements())
            {
                if (tokenizer.nextToken().startsWith(query))
                {
                    return true;
                }
            }

            return false;
        }
    }

    /**
     * Returned when creating a {@link FoundationProject}.
     */
    public class CreateValidationResult extends ServiceResultImpl
    {
        private final String name;
        private final String description;
        private final FoundationProjectEntity entity;

        public CreateValidationResult(final ErrorCollection errorCollection)
        {
            this(errorCollection, null, null, null);
        }

        public CreateValidationResult(final ErrorCollection errorCollection, final String name, final String description, final FoundationProjectEntity entity)
        {
            super(errorCollection);
            this.name = name;
            this.description = description;
            this.entity = entity;
        }

        public String getName()
        {
            return name;
        }

        public String getDescription()
        {
            return description;
        }

        public FoundationProjectEntity getEntity()
        {
            return entity;
        }
    }
}
