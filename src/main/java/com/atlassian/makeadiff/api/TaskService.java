package com.atlassian.makeadiff.api;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.ServiceResultImpl;
import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.bc.issue.IssueService.IssueResult;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueInputParameters;
import com.atlassian.jira.issue.IssueInputParametersImpl;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.jql.builder.JqlClauseBuilder;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.NotNull;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;

/**
 * Service for interacting with the Tasks associated with a {@link FoundationProject}
 */
public class TaskService
{
    private static final Logger log = LoggerFactory.getLogger(TaskService.class);

    private final IssueService issueService;
    private final JiraAuthenticationContext authContext;
    private SearchService searchService;

    public TaskService(IssueService issue,
                       JiraAuthenticationContext jiraAuthenticationContext,
                       SearchService searchService)
    {
        this.authContext = jiraAuthenticationContext;
        this.issueService = issue;
        this.searchService = searchService;
    }

    public CreateValidationResult validateCreateTask(TaskCreationProperties props)
    {
        IssueInputParameters issueInputParameters = new IssueInputParametersImpl();
        issueInputParameters.setProjectId(props.getProjectId());
        issueInputParameters.setIssueTypeId(props.issueTypeId);
        issueInputParameters.setSummary(props.summary);
        issueInputParameters.setReporterId(authContext.getLoggedInUser().getDisplayName());
        issueInputParameters.setAssigneeId(authContext.getLoggedInUser().getDisplayName());
        issueInputParameters.setDescription(props.description);
        issueInputParameters.setEnvironment(props.environment);
        issueInputParameters.setStatusId(props.statusId);
        issueInputParameters.setPriorityId(props.priorityId);
        issueInputParameters.setResolutionId(props.resolutionId);
        issueInputParameters.setSecurityLevelId(props.securityLevelId);
        issueInputParameters.setFixVersionIds(props.fixVersionId);

        User user = authContext.getLoggedInUser();
        IssueService.CreateValidationResult createValidationResult = issueService.validateCreate(
                user, issueInputParameters);

        if (createValidationResult.isValid())
        {
            IssueResult createResult = issueService.create(user, createValidationResult);
            if (createResult.isValid())
            {
                return new CreateValidationResult(new SimpleErrorCollection(), null, null, createValidationResult.getIssue());
            }

        }

        return null;
    }


    public Issue getTask(Long id)
    {
        return issueService.getIssue(authContext.getLoggedInUser(), id).getIssue();
    }

    public List<Issue> getResolvedTasksForProject(@NotNull FoundationProject project)
    {
        JqlClauseBuilder jqlClauseBuilder = JqlQueryBuilder.newClauseBuilder();
        Query query = jqlClauseBuilder.project(project.getKey()).and().resolution().isNotEmpty().buildQuery();

        // A page filter is used to provide pagination. Let's use an unlimited filter to
        // to bypass pagination.
        PagerFilter<?> pagerFilter = PagerFilter.getUnlimitedFilter();
        try
        {
            SearchResults searchResults = searchService.search(authContext.getLoggedInUser(), query, pagerFilter);
            return searchResults.getIssues();
        }
        catch (SearchException e)
        {
            log.error(String.format("Unable to search %s project for resolved issues: %s", project.getKey(), e.getMessage()), e);
        }
        return Collections.emptyList();
    }

    public List<Issue> getIssues(Long projectKey)
    {
        //TODO: need to actually implement on page
        User user = authContext.getLoggedInUser();
        JqlClauseBuilder jqlClauseBuilder = JqlQueryBuilder.newClauseBuilder();
        com.atlassian.query.Query query = jqlClauseBuilder.project(projectKey).buildQuery();
        // A page filter is used to provide pagination. Let's use an unlimited filter to
        // to bypass pagination.
        PagerFilter<?> pagerFilter = PagerFilter.getUnlimitedFilter();
        com.atlassian.jira.issue.search.SearchResults searchResults = null;
        try
        {
            // Perform search results
            searchResults = searchService.search(user, query, pagerFilter);
        }
        catch (SearchException e)
        {
            e.printStackTrace();
        }
        // return the results
        return searchResults.getIssues();
    }

    public List<Issue> getResolvedTasksForUser(@NotNull UserProfile user)
    {
        JqlClauseBuilder jqlClauseBuilder = JqlQueryBuilder.newClauseBuilder();
        Query query = jqlClauseBuilder.status("Resolved").and().resolution().isNotEmpty().and().assigneeUser(user.getUsername()).buildQuery();

        // A page filter is used to provide pagination. Let's use an unlimited filter to
        // to bypass pagination.
        PagerFilter<?> pagerFilter = PagerFilter.getUnlimitedFilter();
        try
        {
            SearchResults searchResults = searchService.search(authContext.getLoggedInUser(), query, pagerFilter);
            return searchResults.getIssues();
        }
        catch (SearchException e)
        {
            log.error(String.format("Unable to get all issues resolved by %s: %s", user.getUsername(), e.getMessage()), e);
        }
        return Collections.emptyList();
    }

    public class CreateValidationResult extends ServiceResultImpl
    {
        private final String name;
        private final String description;
        private final Issue issue;

        public CreateValidationResult(final ErrorCollection errorCollection)
        {
            this(errorCollection, null, null, null);
        }

        public CreateValidationResult(final ErrorCollection errorCollection, final String name, final String description, final Issue issue)
        {
            super(errorCollection);
            this.name = name;
            this.description = description;
            this.issue = issue;
        }

        public String getName()
        {
            return name;
        }

        public String getDescription()
        {
            return description;
        }

        public Issue getIssue()
        {
            return issue;
        }
    }

}
