package com.atlassian.makeadiff.agile;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.plugin.webfragment.contextproviders.AbstractJiraContextProvider;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.web.HttpServletVariables;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.Map;


public class RapidViewMetadataContextProvider extends AbstractJiraContextProvider
{
    @Override
    public Map<String, Object> getContextMap(final User user, final JiraHelper helper)
    {
        final Object agileBoardId = getHttpRequest().getAttribute("agileBoardId");
        if (agileBoardId != null)
        {
            return MapBuilder.<String, Object>newBuilder("agileBoardId", agileBoardId).toHashMap();
        }

        return Collections.emptyMap();
    }

    private HttpServletRequest getHttpRequest()
    {
        return httpVariables().getHttpRequest();
    }

    private HttpServletVariables httpVariables()
    {
        return ComponentAccessor.getComponent(HttpServletVariables.class);
    }
}
