package com.atlassian.makeadiff.action;

import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.makeadiff.api.LabelService;

import java.util.List;
import java.util.Map;

/**
 * Webwork action for the view all projects page.
 */
public class BrowseAllFoundationProjects extends JiraWebActionSupport
{
    private ProjectManager projectManager;
    private LabelService labelService;

    public BrowseAllFoundationProjects(ProjectManager projectManager, LabelService labelService)
    {
        this.projectManager = projectManager;
        this.labelService = labelService;
    }

    public List<Project> getAllProjects()
    {
        return projectManager.getProjectObjects();
    }

    public Map<String, Long> getAllLabelsInProject(Project project)
    {
        return labelService.getAllLabelsInProject(project);
    }


}
