package com.atlassian.makeadiff.action;

import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.makeadiff.api.*;
import com.atlassian.makeadiff.api.FoundationProjectService.ValidationResult;

/**
 * Webwork action for creating projects.
 */
public class CreateFoundationProject extends JiraWebActionSupport
{
    private final FoundationProjectService foundationProjectService;
    private final UserProfileService userProfileService;
    private final GroupHelper groupHelper;

    private ValidationResult validationResult;

    private String key;
    private String name;
    private String description;
    private String url;
    private String location;
    private String repo;

    // TODO add avatar

    public CreateFoundationProject(final FoundationProjectService foundationProjectService, final UserProfileService userProfileService, final GroupHelper groupHelper)
    {
        this.foundationProjectService = foundationProjectService;
        this.userProfileService = userProfileService;
        this.groupHelper = groupHelper;
    }

    @Override
    protected void doValidation()
    {
        final FoundationProjectEntity entity = new FoundationProjectEntity();
        entity.setKey(getKey());
        entity.setRepo(repo);

        // TODO handle case where user may have been logged out since loading the form
        entity.setCreatorUserKey(getLoggedInApplicationUser().getKey());
        entity.setLocation(location);

        validationResult = foundationProjectService.validateCreateProject(getName(), getDescription(), entity, url, location, repo);
        if (!validationResult.isValid())
        {
            addErrorCollection(validationResult.getErrorCollection());
        }
    }

    @Override
    public String doDefault() throws Exception
    {
        if (getLoggedInUser() == null)
        {
            return "notloggedin";
        }

        /*
        if (!belongsToOrganisation())
        {
            return "nopermission";
        }
        */

        if (!groupHelper.isAtlassianStaff(getLoggedInUser()))
        {
            return "nopermission";
        }

        return INPUT;
    }

    @Override
    protected String doExecute() throws Exception
    {
        foundationProjectService.createProject(validationResult);
        return returnCompleteWithInlineRedirect("/browse/" + validationResult.getKey());
    }

    private boolean belongsToOrganisation()
    {
        final ApplicationUser user = this.getLoggedInApplicationUser();
        if (user == null)
        {
            return false;
        }

        final UserProfile userProfile = userProfileService.getUserProfile(user.getKey());
        if (userProfile == null)
        {
            return false;
        }

        return userProfile.belongsToAnyOrganisations();
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return this.description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getKey()
    {
        return key;
    }

    public void setKey(final String key)
    {
        this.key = key;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getLocation()
    {
        return location;
    }

    public void setLocation(String location)
    {
        this.location = location;
    }

    public String getRepo()
    {
        return repo;
    }

    public void setRepo(String repo)
    {
        this.repo = repo;
    }
}
