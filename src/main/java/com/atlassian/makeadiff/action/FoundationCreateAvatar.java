package com.atlassian.makeadiff.action;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarManager;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.bc.user.UserService;
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.SneakyAutoLoginUtil;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.makeadiff.api.*;
import org.apache.commons.lang3.StringUtils;

/**
 * Webwork action for the create avatar page.
 */
public class FoundationCreateAvatar extends JiraWebActionSupport
{
    private final UserProfileService userProfileService;
    private final AvatarManager avatarManager;
    private final AvatarService avatarService;

    private String userKey;
    private UserProfile userProfile;

    public FoundationCreateAvatar(final UserProfileService userProfileService,
                                  final AvatarManager avatarManager,
                                  final AvatarService avatarService)
    {
        this.userProfileService = userProfileService;
        this.avatarManager = avatarManager;
        this.avatarService = avatarService;
    }

    protected String doExecute() throws Exception
    {
        return (getLoggedInApplicationUser() == null) ? "notloggedin" : SUCCESS;
    }

    public String getUsername()
    {
        return (getUserProfile() == null) ? null : getUserProfile().getUsername();
    }

    public String getUserFirstName()
    {
        return getLoggedInApplicationUser().getDisplayName().split(" ")[0];
    }

    public String getUserAvatarUrl()
    {
        return userProfileService.getAvatarURL(getUserProfile(), Avatar.Size.LARGE).toString();
    }

    public Long getUserAvatarId()
    {
        return avatarService.getAvatar(getLoggedInApplicationUser(), getLoggedInApplicationUser()).getId();
    }

    public String getUserKey()
    {
        if (userKey == null)
        {
            userKey = getLoggedInApplicationUser().getKey();
        }
        return userKey;
    }

    public UserProfile getUserProfile()
    {
        if (userProfile == null)
        {
            String userKey = getUserKey();
            if (userKey == null)
                return null;

            userProfile = userProfileService.getUserProfile(userKey);
        }
        return userProfile;
    }

    public Long getDefaultAvatarId()
    {
        return avatarManager.getDefaultAvatarId(Avatar.Type.USER);
    }
}
