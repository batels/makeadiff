package com.atlassian.makeadiff.action;

import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.makeadiff.api.OrganisationService;
import com.atlassian.makeadiff.api.OrganisationService.CreateValidationResult;

/**
 * Webwork action for the create organisation page.
 */
public class CreateOrganisation extends JiraWebActionSupport
{
    private String name;
    private String description;
    private String url;

    private final OrganisationService organisationService;

    private CreateValidationResult validationResult;

    public CreateOrganisation(final OrganisationService organisationService)
    {
        this.organisationService = organisationService;
    }

    @Override
    protected void doValidation()
    {
        //@todo: validation, do it later
//        validationResult = organisationService.validateCreateOrganisation(name, url, null);
//        if (!validationResult.isValid())
//        {
//            addErrorCollection(validationResult.getErrorCollection());
//        }
    }

    @Override
    protected String doExecute() throws Exception
    {
        organisationService.createOrganisation(name, url, description);
        return returnComplete();
    }

    public String getName()
    {
        return name;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getDescription()
    {

        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public void setName(String name)
    {
        this.name = name;
    }
}
