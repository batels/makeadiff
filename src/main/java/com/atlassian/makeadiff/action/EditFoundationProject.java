package com.atlassian.makeadiff.action;

import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.makeadiff.api.FoundationProject;
import com.atlassian.makeadiff.api.FoundationProjectEntity;
import com.atlassian.makeadiff.api.FoundationProjectService;

/**
 * Webwork action for editing projects.
 */
public class EditFoundationProject extends JiraWebActionSupport
{
    private final FoundationProjectService foundationProjectService;

    private String key;
    private FoundationProject project;


    private String name;
    private String description;
    private String url;
    private String location;
    private String repo;

    public EditFoundationProject(final FoundationProjectService foundationProjectService)
    {
        this.foundationProjectService = foundationProjectService;
    }

    @Override
    protected void doValidation()
    {
        // TODO add validateUpdate method to service
    }

    @Override
    public String doDefault() throws Exception
    {
        if (getProject() == null)
        {
            return "notexist";
        }

        if (!foundationProjectService.isAllowedToEdit(getLoggedInApplicationUser(), getProject()))
        {
            return "nopermission";
        }

        name = getProject().getName();
        description = getProject().getDescription();
        url = getProject().getUrl();


        return INPUT;
    }

    @Override
    protected String doExecute() throws Exception
    {
        // Get project from storage
        final FoundationProjectEntity entity = new FoundationProjectEntity();
        entity.setKey(getKey());
        entity.setCreatorUserKey(getProject().getCreator().getKey());
        entity.setStatusKey(getProject().getStatus().getStorageKey());
        entity.setLocation(location);
        entity.setRepo(getRepo());

        FoundationProjectService.ValidationResult validationResult = foundationProjectService.validateCreateProject(getName(), getDescription(), entity, getUrl(), getLocation(), getRepo());

        if (!validationResult.isValid())
        {
            addErrorCollection(validationResult.getErrorCollection());
        }

        foundationProjectService.updateProject(project, validationResult);
        return returnCompleteWithInlineRedirect("/browse/" + getKey());
    }

    public FoundationProject getProject()
    {
        if (project == null)
        {
            project = foundationProjectService.getProject(key);
        }

        return project;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return this.description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getKey()
    {
        return key;
    }

    public void setKey(final String key)
    {
        this.key = key;
    }

    public String getLocation()
    {
        return location;
    }

    public void setLocation(String location)
    {
        this.location = location;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getRepo()
    {
        return repo;
    }

    public void setRepo(String repo)
    {
        this.repo = repo;
    }

}
