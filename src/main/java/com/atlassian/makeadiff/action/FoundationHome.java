package com.atlassian.makeadiff.action;

import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.issue.label.Label;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.makeadiff.api.FoundationProject;
import com.atlassian.makeadiff.api.FoundationProjectService;
import com.atlassian.makeadiff.api.LabelServiceImpl;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Webwork action for the home page.
 */
public class FoundationHome extends JiraWebActionSupport
{
    private final FoundationProjectService projectService;
    private final UserManager userManager;
    private final AvatarService avatarService;

    private List<FoundationProject> featuredProjects;

    public FoundationHome(final FoundationProjectService projectService, final UserManager userManager, final AvatarService avatarService)
    {
        this.projectService = projectService;
        this.userManager = userManager;
        this.avatarService = avatarService;
    }

    @Override
    protected String doExecute() throws Exception
    {
        this.featuredProjects = projectService.getAllProjects(getLoggedInApplicationUser()); // TODO change this back to getFeaturedProjects() once we go live
        return returnComplete();
    }

    public List<FoundationProject> getFeaturedProjects()
    {
        return this.featuredProjects;
    }

    public Double divideDouble(Long a, Integer b)
    {
        if ((new Integer(0)).equals(b))
        {
            return 0.0;
        }
        else
        {
            // x = new Double(a);
            return (2 * a.doubleValue() / b.doubleValue());
        }
    }

    public URI getAvatarUrl(final String userKey)
    {
        ApplicationUser targetUser = userManager.getUserByKey(userKey);
        return avatarService.getAvatarURL(getLoggedInApplicationUser(), targetUser, Avatar.Size.SMALL);
    }
}
