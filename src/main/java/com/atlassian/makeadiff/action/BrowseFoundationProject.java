package com.atlassian.makeadiff.action;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.plugin.webresource.JiraWebResourceManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.template.VelocityTemplatingEngine;
import com.atlassian.jira.util.JiraVelocityUtils;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.makeadiff.api.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.velocity.exception.VelocityException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.atlassian.jira.template.TemplateSources.file;

/**
 * Webwork action for the project page.
 */
public class BrowseFoundationProject extends JiraWebActionSupport
{
    private static final String DEFAULT_TAB = "overview";

    private final FoundationProjectService foundationProjectService;
    private final VelocityTemplatingEngine velocityTemplatingEngine;
    private final JiraAuthenticationContext authenticationContext;
    private final JiraWebResourceManager webResourceManager;
    private final TaskService taskService;
    private final IssueManager issueManager;
    private final ProjectImageService projectImageService;
    private final PermissionManager permissionManager;

    private FoundationProject project;
    private boolean contentOnly;
    private String selectedTab;
    private String key;

    public BrowseFoundationProject(final FoundationProjectService foundationProjectService,
                                   final VelocityTemplatingEngine velocityTemplatingEngine,
                                   final JiraAuthenticationContext authenticationContext,
                                   final JiraWebResourceManager webResourceManager,
                                   final TaskService taskService,
                                   final IssueManager issueManager,
                                   final ProjectImageService projectImageService,
                                   final PermissionManager permissionManager)
    {
        this.foundationProjectService = foundationProjectService;
        this.velocityTemplatingEngine = velocityTemplatingEngine;
        this.authenticationContext = authenticationContext;
        this.webResourceManager = webResourceManager;
        this.taskService = taskService;
        this.issueManager = issueManager;
        this.projectImageService = projectImageService;
        this.permissionManager = permissionManager;
    }

    @Override
    protected String doExecute() throws Exception
    {
        webResourceManager.putMetadata("projectKey", key);
        return super.doExecute();
    }

    public FoundationProject getProject()
    {
        if (project == null)
        {
            project = foundationProjectService.getProject(key);
        }
        return project;
    }

    public boolean isCurrentUserChampion()
    {
        return foundationProjectService.isUserChampion(getLoggedInApplicationUser(), getProject());
    }

    public boolean isExists()
    {
        return (getProject() != null);
    }

    public String getKey()
    {
        return key;
    }

    public void setKey(final String key)
    {
        this.key = key;
    }

    public String getName()
    {
        return getProject().getName();
    }

    public boolean isContentOnly()
    {
        return contentOnly;
    }

    public void setContentOnly(final boolean contentOnly)
    {
        this.contentOnly = contentOnly;
    }

    public String getSelectedTab()
    {
        return (StringUtils.isBlank(selectedTab)) ? DEFAULT_TAB : selectedTab;
    }

    public void setSelectedTab(final String selectedTab)
    {
        this.selectedTab = selectedTab;
    }

    public String getTabContentWithHtml()
    {
        final Map<String, Object> velocityParams = createVelocityParams();

        try
        {
            return velocityTemplatingEngine.render(file("templates/project/project-tab-" + getSelectedTab() + ".vm")).applying(velocityParams).asHtml();
        }
        catch (VelocityException e)
        {
            log.error(e);
            return "";
        }
    }

    public List<Issue> getAllTasks()
    {
        final ArrayList<Long> issueIds = new ArrayList<Long>();
        for (Issue issue : taskService.getIssues(project.getId()))
        {
            issueIds.add(issue.getId());
        }

        return (issueIds.isEmpty()) ? Collections.<Issue>emptyList() : issueManager.getIssueObjects(issueIds);
    }

    public List<Issue> getUnresolvedTasks()
    {
        final ArrayList<Long> issueIds = new ArrayList<Long>();
        for (Issue issue : taskService.getIssues(project.getId()))
        {
            if (issue.getResolutionObject() == null)
            {
                issueIds.add(issue.getId());
            }
        }

        return (issueIds.isEmpty()) ? Collections.<Issue>emptyList() : issueManager.getIssueObjects(issueIds);
    }

    public List<FoundationProjectContributor> getRemainingContributors(int start)
    {
        List<FoundationProjectContributor> contributors = project.getContributors();

        if (start < contributors.size())
        {
            return contributors.subList(start, contributors.size());
        }
        else
        {
            return null;
        }
    }

    public List<FoundationProjectContributor> getSomeContributors(int start, int end)
    {
        List<FoundationProjectContributor> contributors = project.getContributors();
        if (end < contributors.size())
        {
            return contributors.subList(start, end);
        }
        else
        {
            return contributors.subList(start, contributors.size());
        }

    }

    public List<FoundationProjectContributor> getContributors()
    {
        return project.getContributors();
    }

    public boolean getHasCreateIssuePermission()
    {
        return permissionManager.hasPermission(Permissions.CREATE_ISSUE, getProject(), getLoggedInApplicationUser());
    }

    public boolean getHasProjectAdminPermission()
    {
        return permissionManager.hasPermission(Permissions.PROJECT_ADMIN, getProject(), getLoggedInApplicationUser());
    }

    private Map<String, Object> createVelocityParams()
    {
        final Map<String, Object> velocityParams = JiraVelocityUtils.getDefaultVelocityParams(authenticationContext);
        velocityParams.put("action", this);
        return velocityParams;

    }

    public boolean isAllowedToEdit()
    {
        return foundationProjectService.isAllowedToEdit(getLoggedInApplicationUser(), getProject());
    }

    public boolean isHasProjectImage()
    {
        return projectImageService.hasBannerImage(key);
    }
}
