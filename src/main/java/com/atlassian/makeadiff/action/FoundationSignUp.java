package com.atlassian.makeadiff.action;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.user.UserService;
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.SneakyAutoLoginUtil;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.makeadiff.api.GroupHelper;
import com.atlassian.makeadiff.api.Organisation;
import com.atlassian.makeadiff.api.OrganisationService;
import com.atlassian.makeadiff.api.UserProfileService;
import org.apache.commons.lang3.StringUtils;

/**
 * Webwork action for the sign up page.
 */
public class FoundationSignUp extends JiraWebActionSupport
{
    private final UserService userService;
    private final UserManager userManager;
    private final UserProfileService userProfileService;
    private final OrganisationService organisationService;
    private final GroupHelper groupHelper;

    private UserService.CreateUserValidationResult result;

    private String fullname;
    private String email;
    private String password;
    private boolean belongsToOrg;
    private String organisation;

    public FoundationSignUp(final UserService userService, final UserManager userManager,
                            final UserProfileService userProfileService, final OrganisationService organisationService, final GroupHelper groupHelper)
    {
        this.userService = userService;
        this.userManager = userManager;
        this.userProfileService = userProfileService;
        this.organisationService = organisationService;
        this.groupHelper = groupHelper;
    }

    @Override
    public String doDefault() throws Exception
    {
        if (getLoggedInUser() != null)
        {
            return "alreadyloggedin";
        }

        return super.doDefault();
    }

    @Override
    protected void doValidation()
    {
        if (getLoggedInUser() != null)
        {
            return;
        }

        result = userService.validateCreateUserForSignup(
                getLoggedInUser(), getEmail(), getPassword(), getPassword(), getEmail(), getFullname());

        if (!result.isValid())
        {
            addErrorCollection(result.getErrorCollection());
        }

        // If the user has specified that they belong to a organisation, they must say which one
        if (belongsToOrg && StringUtils.isBlank(organisation))
        {
            addError("organisation", getText("makeadiff.signup.form.organisation.required"));
        }
    }

    @Override
    protected String doExecute() throws Exception
    {
        if (getLoggedInUser() != null)
        {
            return "alreadyloggedin";
        }

        try
        {
            final User user = userService.createUserFromSignup(result);
            if (user != null)
            {
                createUserProfile(user);
                logUserInAutomatically(user);
                groupHelper.addUserToAppropriateGroups(user);
            }
            else
            {
                addErrorMessage(getText("signup.error.duplicateuser"));
            }
        }
        catch (final CreateException e)
        {
            log.error("Error creating user from public sign up", e);
            return "systemerror";
        }

        setReturnUrl("/view/signup/avatar");
        return returnComplete(getReturnUrl());
    }

    private void createUserProfile(final User user)
    {
        // Get application user so that we have the user key
        final ApplicationUser appUser = userManager.getUserByName(user.getName());

        if (belongsToOrg)
        {
            final Organisation org = organisationService.createOrUpdate(organisation, null);
            organisationService.addUserToOrganisation(appUser.getKey(), org);
        }

        userProfileService.createUserProfile(appUser.getKey(), null, null);
    }

    private boolean logUserInAutomatically(final User user)
    {
        try
        {
            return SneakyAutoLoginUtil.logUserIn(user.getName(), request);
        }
        catch (final Exception e)
        {
            log.warn("Error with automatic login after sign up. The user will need to login in manually.", e);
        }

        return false;
    }

    public String getFullname()
    {
        return fullname;
    }

    public void setFullname(final String fullname)
    {
        this.fullname = fullname;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(final String email)
    {
        this.email = email;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(final String password)
    {
        this.password = password;
    }

    public boolean isBelongsToOrg()
    {
        return belongsToOrg;
    }

    public void setBelongsToOrg(final boolean belongsToOrg)
    {
        this.belongsToOrg = belongsToOrg;
    }

    public String getOrganisation()
    {
        return organisation;
    }

    public void setOrganisation(final String organisation)
    {
        this.organisation = organisation;
    }
}
