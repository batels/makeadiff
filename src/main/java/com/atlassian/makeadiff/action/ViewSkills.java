package com.atlassian.makeadiff.action;

import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.jql.builder.JqlClauseBuilder;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.makeadiff.api.LabelService;
import com.atlassian.query.Query;
import com.google.common.base.Function;
import com.google.common.base.Objects;
import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Webwork action to list all contributors of a project
 */
public class ViewSkills extends JiraWebActionSupport
{
    private ProjectManager projectManager;
    private LabelService labelService;
    private SearchService searchService;

    public ViewSkills(ProjectManager projectManager, LabelService labelService, SearchService searchService)
    {
        this.projectManager = projectManager;
        this.labelService = labelService;
        this.searchService = searchService;
    }

    public String[] getQueryString()
    {
        String temp = Objects.firstNonNull(getHttpRequest().getParameter("searchSkills"), "");
        String[] skills = temp.split("\\s*?,\\s*?");
        if (skills.length == 1 && skills[0].equals(""))
        {
            return new String[0];
        }
        return skills;
    }

    public Map<Project, Collection<Issue>> getProjects()
    {
        return Multimaps.index(getIssues(getQueryString()), new Function<Issue, Project>()
        {
            @Override
            public Project apply(Issue issue)
            {
                return issue.getProjectObject();
            }
        }).asMap();
    }

    private Collection<Issue> getIssues(String[] madSkills)
    {
        JqlClauseBuilder clauseBuilder = JqlQueryBuilder.newClauseBuilder();
        Query query;
        if (madSkills.length == 0)
        {
            // return all the issues that ever existed and the project will be determined auto
            query = clauseBuilder.buildQuery();
        }
        else
        {
            query = clauseBuilder.labels(madSkills).buildQuery();
        }

        PagerFilter<?> pagerFilter = PagerFilter.getUnlimitedFilter();
        com.atlassian.jira.issue.search.SearchResults searchResults = null;
        try
        {
            // Perform search results
            searchResults = searchService.search(getLoggedInUser(), query, pagerFilter);
        }
        catch (SearchException e)
        {
            e.printStackTrace();
        }
        // return the results

        return (searchResults != null ? searchResults.getIssues() : new ArrayList<Issue>());
    }

    // Returns a list of skill sets and for each of them projects with that skill set
    public Map<String, Collection<Project>> listProjectsUnderSkills()
    {
        List<Project> allProjects = getAllProjects();
        Multimap<String, Project> label2project = LinkedListMultimap.create();
        for (Project p : allProjects)
        {
            for (String label : labelService.getAllLabelsInProject(p).keySet())
            {
                label2project.put(label, p);
            }
        }
        return label2project.asMap();
    }

    public List<Project> getAllProjects()
    {
        return projectManager.getProjectObjects();
    }
}