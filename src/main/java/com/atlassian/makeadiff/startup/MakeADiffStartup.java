package com.atlassian.makeadiff.startup;

import com.atlassian.crowd.exception.OperationNotPermittedException;
import com.atlassian.crowd.exception.embedded.InvalidGroupException;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.config.properties.LookAndFeelBean;
import com.atlassian.jira.config.util.JiraHome;
import com.atlassian.jira.exception.AddException;
import com.atlassian.jira.exception.PermissionException;
import com.atlassian.makeadiff.api.GroupHelper;
import com.atlassian.makeadiff.api.workflow.SimplifiedWorkflowService;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import org.apache.commons.lang.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * Runs on start up to apply any Make a Diff customisations.
 */
public class MakeADiffStartup implements LifecycleAware
{
    private static final String JIRA_SCALED_LOGO_FILENAME = "jira-logo-scaled.png";                     // Copied from com.atlassian.jira.lookandfeel.LookAndFeelConstants
    private static final String LOGO_OUTPUT_FORMAT = "png";                                             // Copied from com.atlassian.jira.lookandfeel.upload.LogoUploader
    private static final String USING_CUSTOM_LOGO = "com.atlassian.jira.lookandfeel:usingCustomLogo";   // Copied from com.atlassian.jira.lookandfeel.LookAndFeelConstants

    private static final Logger log = LoggerFactory.getLogger(MakeADiffStartup.class);

    private final ApplicationProperties applicationProperties;
    private final JiraHome jiraHome;
    private final PluginSettings globalSettings;
    private final SimplifiedWorkflowService simplifiedWorkflowService;
    private final GroupHelper groupHelper;

    public MakeADiffStartup(final ApplicationProperties applicationProperties, final JiraHome jiraHome, final PluginSettingsFactory pluginSettingsFactory, final SimplifiedWorkflowService simplifiedWorkflowService, final GroupHelper groupHelper)
    {
        this.applicationProperties = applicationProperties;
        this.jiraHome = jiraHome;
        this.simplifiedWorkflowService = simplifiedWorkflowService;
        this.groupHelper = groupHelper;
        this.globalSettings = pluginSettingsFactory.createGlobalSettings();
    }

    @Override
    public void onStart()
    {
        configureLookAndFeel();
        configureLogo();
        simplifiedWorkflowService.ensureDefaultWorkflowSchemeExists();
        configureAtlassianUserGroup();
    }

    private void configureLookAndFeel()
    {
        this.applicationProperties.setString(APKeys.JIRA_LF_TOP_BGCOLOUR, "#EDE9E3");
        this.applicationProperties.setString(APKeys.JIRA_LF_TOP_HIGHLIGHTCOLOR, "#EDE9E3");
        this.applicationProperties.setString(APKeys.JIRA_LF_TOP_SEPARATOR_BGCOLOR, "#A39E98");
        this.applicationProperties.setString(APKeys.JIRA_LF_TOP_TEXTCOLOUR, "#59544E");
        this.applicationProperties.setString(APKeys.JIRA_LF_TOP_TEXTHIGHLIGHTCOLOR, "#0088E4");
        this.applicationProperties.setString(APKeys.JIRA_LF_MENU_BGCOLOUR, "#A0D0F7");
        this.applicationProperties.setString(APKeys.JIRA_LF_MENU_TEXTCOLOUR, "#59544E");
        this.applicationProperties.setString(APKeys.JIRA_LF_HERO_BUTTON_BASEBGCOLOUR, "#0088E4");
        this.applicationProperties.setString(APKeys.JIRA_LF_HERO_BUTTON_TEXTCOLOUR, "#FFFFFF");
        this.applicationProperties.setString(APKeys.JIRA_LF_TEXT_ACTIVE_LINKCOLOUR, "#006BB3");
        this.applicationProperties.setString(APKeys.JIRA_LF_TEXT_HEADINGCOLOUR, "#59544E");
        this.applicationProperties.setString(APKeys.JIRA_LF_TEXT_LINKCOLOUR, "#006BB3");

        // this causes the underlying counter to be bumped
        final LookAndFeelBean lookAndFeelBean = LookAndFeelBean.getInstance(applicationProperties);
        lookAndFeelBean.updateVersion(0);
    }

    private void configureLogo()
    {
        try
        {
            final InputStream srcStream = getClass().getResourceAsStream("/images/makeadiff-logo.png");
            final File dstFile = new File(getLogoDirectory(), JIRA_SCALED_LOGO_FILENAME);
            final BufferedImage image = ImageIO.read(srcStream);
            ImageIO.write(image, LOGO_OUTPUT_FORMAT, dstFile);

            final LookAndFeelBean lookAndFeelBean = LookAndFeelBean.getInstance(applicationProperties);
            lookAndFeelBean.setLogoWidth(String.valueOf(image.getWidth()));
            lookAndFeelBean.setLogoHeight(String.valueOf(image.getHeight()));
            lookAndFeelBean.setLogoUrl("/" + JIRA_SCALED_LOGO_FILENAME);
            globalSettings.put(USING_CUSTOM_LOGO, BooleanUtils.toStringTrueFalse(true));
        }
        catch (final IOException e)
        {
            log.error("Error setting logo", e);
        }
    }

    private void configureAtlassianUserGroup()
    {
        try
        {
            groupHelper.ensureAtlassianGroupExists();
        }
        catch (OperationNotPermittedException e)
        {
            log.error("Error creating atlassian user group", e);
        }
        catch (InvalidGroupException e)
        {
            log.error("Error creating atlassian user group", e);
        }

        try
        {
            groupHelper.addAllAtlassianUsersToAtlassianGroup();
        }
        catch (AddException e)
        {
            log.error("Error adding users to atlassian user group", e);
        }
        catch (PermissionException e)
        {
            log.error("Error adding users to atlassian user group", e);
        }
    }

    /*
     * Copied from com.atlassian.jira.lookandfeel.upload.UploadHelper
     */
    private File getLogoDirectory()
    {
        String logoDirectoryName = jiraHome.getHomePath() + "/logos";
        File logoDirectory = new File(logoDirectoryName);
        if (!logoDirectory.exists())
        {
            logoDirectory.mkdirs();
        }
        return logoDirectory;
    }
}
