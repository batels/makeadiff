package com.atlassian.makeadiff.ao;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.makeadiff.api.Organisation;
import net.java.ao.Query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * Active objects implementation of the {@link com.atlassian.makeadiff.api.OrganisationStore}.
 */
public class AOOrganisationStoreImpl implements AOOrganisationStore
{
    private final ActiveObjects activeObjects;
    private final UserUtil userUtil;
    private final GroupManager groupManager;

    public AOOrganisationStoreImpl(final ActiveObjects activeObjects, final UserUtil userUtil, final GroupManager groupManager)
    {
        this.activeObjects = activeObjects;
        this.userUtil = userUtil;
        this.groupManager = groupManager;
    }

    @Override
    public Organisation getOrganisationById(final int id)
    {
        final AOOrganisationEntity[] entities = activeObjects.find(AOOrganisationEntity.class, "ID = ?", id);

        // We should have 0 or 1 results
        if (entities.length == 0)
        {
            return null;
        }

        return convert(entities[0]);
    }

    @Override
    public Organisation getOrganisationByKey(final String key)
    {
        final AOOrganisationEntity[] entities = activeObjects.find(AOOrganisationEntity.class, "KEY = ?", key);

        // We should have 0 or 1 results
        if (entities.length == 0)
        {
            return null;
        }

        return convert(entities[0]);
    }

    @Override
    public Organisation getOrganisationByName(final String name)
    {
        final AOOrganisationEntity[] entities = activeObjects.find(AOOrganisationEntity.class, "NAME = ?", name);

        // We should have 0 or 1 results
        if (entities.length == 0)
        {
            return null;
        }

        return convert(entities[0]);

    }

    public Collection<Organisation> findOrganisationByLikeName(final String likeName)
    {
        return convertToCollection(activeObjects.find(AOOrganisationEntity.class, "LOWER(\"NAME\") LIKE ?", "%" + likeName.toLowerCase() + "%"));
    }

    @Override
    public Collection<Organisation> getAllOrganisations()
    {
        return convertToCollection(activeObjects.find(AOOrganisationEntity.class));
    }

    public Collection<Organisation> getAllOrganisations(final int limit)
    {
        return convertToCollection(activeObjects.find(AOOrganisationEntity.class, Query.select().limit(limit)));
    }

    @Override
    public Organisation createOrUpdateOrganisation(final String name, final String url, final String description, final boolean verified)
    {
        final AOOrganisationEntity[] entities = activeObjects.find(AOOrganisationEntity.class, "NAME = ?", name);
        final AOOrganisationEntity entity;
        if (entities.length != 0)
        {
            // Update
            entity = entities[0];
            entity.setUrl(url);
            entity.setDescription(description);
            entity.setVerified(verified);
            entity.save();
        }
        else
        {
            // Create
            entity = activeObjects.create(AOOrganisationEntity.class);
            entity.setKey(generateKey(name));
            entity.setName(name);
            entity.setUrl(url);
            entity.setDescription(description);
            entity.setVerified(false);
            entity.save();
        }

        return convert(entity);
    }

    @Override
    public Collection<Organisation> getOrganisationsForUser(final String userKey)
    {
        final ApplicationUser user = userUtil.getUserByKey(userKey);
        if (user == null)
        {
            return Collections.emptyList();
        }

        final Collection<Organisation> result = new ArrayList<Organisation>();
        for (final Group group : groupManager.getGroupsForUser(user.getName()))
        {
            final Organisation org = getOrganisationByName(group.getName());
            if (org != null)
            {
                result.add(org);
            }
        }
        return result;
    }

    private static String generateKey(final String name)
    {
        return name;
    }

    private static Organisation convert(final AOOrganisationEntity entity)
    {
        return new Organisation(entity.getID(), entity.getKey(), entity.getName(), entity.getUrl(), entity.getDescription(), entity.getVerified());
    }

    private static Collection<Organisation> convertToCollection(final AOOrganisationEntity[] entities)
    {
        final Collection<Organisation> result = new ArrayList<Organisation>(entities.length);

        for (final AOOrganisationEntity entity : entities)
        {
            result.add(convert(entity));
        }

        return result;
    }
}
