package com.atlassian.makeadiff.ao;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.makeadiff.api.Organisation;
import com.atlassian.makeadiff.api.OrganisationStore;
import com.atlassian.makeadiff.api.UserProfile;
import com.atlassian.makeadiff.api.UserProfileStore;

import java.util.Collection;

/**
 * Active objects implementation of the {@link UserProfileStore}.
 */
public class AOUserProfileStoreImpl implements AOUserProfileStore
{
    private final ActiveObjects activeObjects;
    private final OrganisationStore organisationStore;

    public AOUserProfileStoreImpl(final ActiveObjects activeObjects, final OrganisationStore organisationStore)
    {
        this.activeObjects = activeObjects;
        this.organisationStore = organisationStore;
    }

    @Override
    public UserProfile getUserProfile(final String key)
    {
        AOUserProfileEntity aoEntity = getByKey(key);
        if (aoEntity == null)
        {
            aoEntity = activeObjects.create(AOUserProfileEntity.class);
            aoEntity.setKey(key);
            aoEntity.setSkills(null);
            aoEntity.setDescription(null);
            aoEntity.save();
        }
        return convert(aoEntity);
    }

    private AOUserProfileEntity getByKey(String key)
    {
        final AOUserProfileEntity[] entities = activeObjects.find(AOUserProfileEntity.class, "KEY = ?", key);

        // We should have 0 or 1 results
        if (entities.length == 0)
        {
            return null;
        }

        return entities[0];
    }

    @Override
    public void createUserProfile(final String key, String skills, String description)
    {
        final AOUserProfileEntity entity = activeObjects.create(AOUserProfileEntity.class);
        entity.setKey(key);
        entity.setSkills(skills);
        entity.setDescription(description);
        entity.save();
    }

    @Override
    public void updateUserProfile(UserProfile userProfile)
    {
        final AOUserProfileEntity aoUserProfileEntity = getByKey(userProfile.getKey());
        aoUserProfileEntity.setSkills(userProfile.getSkills());
        aoUserProfileEntity.setDescription(userProfile.getDescription());

        aoUserProfileEntity.save();
    }

    private UserProfile convert(final AOUserProfileEntity entity)
    {
        final Collection<Organisation> organisations = organisationStore.getOrganisationsForUser(entity.getKey());
        return new UserProfile(entity.getKey(), organisations, entity.getSkills(), entity.getDescription());
    }
}
