package com.atlassian.makeadiff.ao;

import com.atlassian.activeobjects.tx.Transactional;
import com.atlassian.makeadiff.api.OrganisationStore;

@Transactional
public interface AOOrganisationStore extends OrganisationStore
{
}
