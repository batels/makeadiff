package com.atlassian.makeadiff.ao;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.makeadiff.api.FoundationProjectEntity;
import com.atlassian.makeadiff.api.FoundationProjectStore;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Active objects implementation of the {@link FoundationProjectStore}.
 */
public class AOFoundationProjectStoreImpl implements AOFoundationProjectStore
{
    // ------------------------------------------------------------------------------------------------------- Constants
    // ------------------------------------------------------------------------------------------------- Type Properties
    // ---------------------------------------------------------------------------------------------------- Dependencies
    private final ActiveObjects activeObjects;

    // ---------------------------------------------------------------------------------------------------- Constructors
    public AOFoundationProjectStoreImpl(final ActiveObjects activeObjects)
    {
        this.activeObjects = activeObjects;
    }

    // -------------------------------------------------------------------------------------------------- Public Methods
    @Override
    public FoundationProjectEntity getProject(final String projectKey)
    {
        final AOFoundationProjectEntity[] entities = activeObjects.find(AOFoundationProjectEntity.class, "KEY = ?", projectKey);

        // We should have 0 or 1 results
        if (entities.length == 0)
        {
            return null;
        }

        return convertFromAO(entities[0]);
    }

    @Override
    public Collection<FoundationProjectEntity> getAllProjects()
    {
        return convertToCollection(activeObjects.find(AOFoundationProjectEntity.class));
    }

    @Override
    public void addProject(FoundationProjectEntity project)
    {
        final AOFoundationProjectEntity to = activeObjects.create(AOFoundationProjectEntity.class);
        copyPropertiesFromEntityToAo(project, to);
        to.save();
    }

    @Override
    public void updateProject(FoundationProjectEntity project)
    {
        final AOFoundationProjectEntity[] entities = activeObjects.find(AOFoundationProjectEntity.class, "KEY = ?", project.getKey());

        // TODO: handle this properly.
        if (entities.length == 0)
        {
            return;
        }

        final AOFoundationProjectEntity aoProject = entities[0];
        copyPropertiesFromEntityToAo(project, aoProject);
        aoProject.save();
    }


    // -------------------------------------------------------------------------------------------------- Helper Methods

    private static FoundationProjectEntity convertFromAO(final AOFoundationProjectEntity from)
    {
        final FoundationProjectEntity to = new FoundationProjectEntity();
        copyPropertiesFromAoToEntity(from, to);

        return to;
    }

    private static Collection<FoundationProjectEntity> convertToCollection(final AOFoundationProjectEntity[] entities)
    {
        final Collection<FoundationProjectEntity> result = new ArrayList<FoundationProjectEntity>(entities.length);

        for (final AOFoundationProjectEntity entity : entities)
        {
            result.add(convertFromAO(entity));
        }

        return result;
    }

    private static void copyPropertiesFromAoToEntity(AOFoundationProjectEntity aoProject, FoundationProjectEntity project)
    {
        project.setKey(aoProject.getKey());
        project.setCreatorUserKey(aoProject.getCreatorUserKey());
        project.setStatusKey(aoProject.getStatusKey());
        project.setLocation(aoProject.getLocation());
        project.setRepo(aoProject.getRepo());
        project.setAgileBoardId(aoProject.getAgileBoardId());
    }

    private void copyPropertiesFromEntityToAo(FoundationProjectEntity project, AOFoundationProjectEntity aoProject)
    {
        aoProject.setKey(project.getKey());
        aoProject.setCreatorUserKey(project.getCreatorUserKey());
        aoProject.setStatusKey(project.getStatusKey());
        aoProject.setLocation(project.getLocation());
        aoProject.setRepo(project.getRepo());
        aoProject.setAgileBoardId(project.getAgileBoardId());
    }
}
