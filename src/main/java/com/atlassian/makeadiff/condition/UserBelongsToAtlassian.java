package com.atlassian.makeadiff.condition;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.makeadiff.api.GroupHelper;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.Condition;

import java.util.Map;

public class UserBelongsToAtlassian implements Condition
{
    private final JiraAuthenticationContext authenticationContext;
    private final GroupHelper groupHelper;

    public UserBelongsToAtlassian(final JiraAuthenticationContext authenticationContext, final GroupHelper groupHelper)
    {
        this.authenticationContext = authenticationContext;
        this.groupHelper = groupHelper;
    }


    @Override
    public void init(Map<String, String> stringStringMap) throws PluginParseException
    {
    }

    @Override
    public boolean shouldDisplay(Map<String, Object> stringObjectMap)
    {
        return authenticationContext.getUser() != null &&
                groupHelper.isAtlassianStaff(authenticationContext.getUser().getDirectoryUser());
    }
}
