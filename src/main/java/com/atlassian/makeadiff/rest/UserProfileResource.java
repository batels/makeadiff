package com.atlassian.makeadiff.rest;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.makeadiff.api.Organisation;
import com.atlassian.makeadiff.api.OrganisationService;
import com.atlassian.makeadiff.api.UserProfile;
import com.atlassian.makeadiff.api.UserProfileService;
import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.annotate.JsonProperty;

import javax.annotation.Nullable;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Collection;
import java.util.Collections;


@Path("profile")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})

public class UserProfileResource extends BaseResource
{
    private final UserProfileService userProfileService;
    private final OrganisationService organisationService;
    private final JiraAuthenticationContext authenticationContext;

    public UserProfileResource(final UserProfileService userProfileService, final OrganisationService organisationService, final JiraAuthenticationContext authenticationContext)
    {
        this.userProfileService = userProfileService;
        this.organisationService = organisationService;
        this.authenticationContext = authenticationContext;
    }

    @GET
    public Response get()
    {
        final UserProfile userProfile = getUserProfile();
        if (userProfile == null)
        {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok(toBean(userProfile)).build();
    }

    @PUT
    @Path("skills")
    public Response setSkills(final String skills)
    {
        final UserProfile userProfile = getUserProfile();
        if (userProfile == null)
        {
            return Response.status(Response.Status.NOT_FOUND).build();
        }

        userProfile.setSkills(skills);
        userProfileService.updateUserProfile(userProfile);
        return Response.ok().build();
    }

    @PUT
    @Path("description")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response setDescription(final String description)
    {
        final UserProfile userProfile = getUserProfile();
        if (userProfile == null)
        {
            return Response.status(Response.Status.NOT_FOUND).build();
        }

        userProfile.setDescription(description);
        userProfileService.updateUserProfile(userProfile);
        return Response.ok(toBean(userProfile)).build();
    }

    @PUT
    @Path("org")
    public Response addOrganisation(final String orgName)
    {
        final UserProfile userProfile = getUserProfile();
        if (userProfile == null)
        {
            return Response.status(Response.Status.NOT_FOUND).build();
        }

        if (StringUtils.isBlank(orgName))
        {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        final Organisation org = organisationService.createOrUpdate(orgName, null);
        organisationService.addUserToOrganisation(userProfile.getKey(), org);
        return Response.ok().build();
    }

    @DELETE
    @Path("org")
    public Response removeOrganisation(final String orgName)
    {
        final UserProfile userProfile = getUserProfile();
        if (userProfile == null)
        {
            return Response.status(Response.Status.NOT_FOUND).build();
        }

        if (StringUtils.isBlank(orgName))
        {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        final Organisation org = organisationService.getOrganisationByName(orgName);
        organisationService.removeUserFromOrganisation(userProfile.getKey(), org);
        return Response.ok().build();
    }

    private UserProfile getUserProfile()
    {
        return userProfileService.getUserProfile(authenticationContext.getUser().getKey());
    }

    private static UserProfileBean toBean(final UserProfile userProfile)
    {
        UserProfileBean bean = new UserProfileBean();
        bean.key = userProfile.getKey();
        bean.organisations = getOrganisationNames(userProfile.getOrganisations());
        bean.skills = userProfile.getSkills();
        bean.description = userProfile.getDescription();
        return bean;
    }

    private static Collection<String> getOrganisationNames(final Collection<Organisation> organisations)
    {
        if (organisations == null || organisations.isEmpty())
        {
            return Collections.emptyList();
        }

        return Collections2.transform(organisations, new Function<Organisation, String>()
        {
            @Override
            public String apply(@Nullable final Organisation org)
            {
                return org.getName();
            }
        });
    }

    public static class UserProfileBean
    {
        @JsonProperty
        public String key;

        @JsonProperty
        public Collection<String> organisations;

        @JsonProperty
        public String skills;

        @JsonProperty
        public String description;
    }
}
