package com.atlassian.makeadiff.rest;

import org.codehaus.jackson.annotate.JsonProperty;

public class ProjectBean
{
    @JsonProperty
    public Long id;

    @JsonProperty
    public String key;

    @JsonProperty
    public String name;

    @JsonProperty
    public Boolean hasCreateIssuePermission;

    @JsonProperty
    public Boolean hasProjectAdminPermission;

    public ProjectBean() {}

    public ProjectBean(final Long id, final String key, final String name, final Boolean hasCreateIssuePermission,
        final Boolean hasProjectAdminPermission)
    {
        this.id = id;
        this.key = key;
        this.name = name;
        this.hasCreateIssuePermission = hasCreateIssuePermission;
        this.hasProjectAdminPermission = hasProjectAdminPermission;
    }
}
