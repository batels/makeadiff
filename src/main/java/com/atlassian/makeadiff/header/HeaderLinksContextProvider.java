package com.atlassian.makeadiff.header;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.plugin.webfragment.SimpleLinkManager;
import com.atlassian.jira.plugin.webfragment.contextproviders.AbstractJiraContextProvider;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.plugin.webfragment.model.SimpleLink;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.collect.MapBuilder;

import java.util.List;
import java.util.Map;

public class HeaderLinksContextProvider extends AbstractJiraContextProvider
{
    static final String HEADER_LINKS_SECTION = "makeadiff.headerlinks";

    private final SimpleLinkManager simpleLinkManager;
    private final JiraAuthenticationContext authenticationContext;

    public HeaderLinksContextProvider(final SimpleLinkManager simpleLinkManager, final JiraAuthenticationContext authenticationContext)
    {
        this.simpleLinkManager = simpleLinkManager;
        this.authenticationContext = authenticationContext;
    }

    @Override
    public Map<String, Object> getContextMap(final User user, final JiraHelper helper)
    {
        return MapBuilder.<String, Object>newBuilder("links", getLinks(helper)).toHashMap();
    }

    private List<SimpleLink> getLinks(final JiraHelper helper)
    {
        final User user = authenticationContext.getLoggedInUser();
        return simpleLinkManager.getLinksForSection(HEADER_LINKS_SECTION, user, helper);
    }
}
