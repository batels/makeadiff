(function($) {

    AJS.toInit(function($) {

        // Add sign up button if not logged in
        var username = AJS.Meta.get("remote-user");
        if (!username) {
            var $signUpButton = $("<a/>")
                .addClass("aui-button")
                .addClass("aui-style")
                .addClass("aui-button-primary")
                .attr("href", contextPath + "/view/signup")
                .text(AJS.I18n.getText("makeadiff.signup.button.label"));

            $(".aui-header-secondary ul.aui-nav")
                .append($signUpButton);
        }

        // Footer logo
        $("#page > #footer").append($("<div/>")
            .append($("<span/>")
                .addClass("powered-by")
                .text("Powered by"))
            .append($("<img/>")
                .addClass("atlassian-foundation-logo")
                .attr("src", contextPath + "/download/resources/com.atlassian.makeadiff.makeadiff-jira-plugin/images/foundation-greytext_xsmall_trans_atlassian.png")));


        // Fix agile board ids for projects
        setTimeout(function() {
            if (location.pathname.indexOf("secure/ManageRapidViews.jspa") > -1) {
                $("td[headers='ghx-rv-name']").each(function() {
                    var $el = $(this);
                    var projectKey = $el.text().match(/\((.*)\)/)[1];
                    $.ajax({
                        url: contextPath + "/rest/makeadiff/1/projects/" + projectKey + "/agileboard",
                        success: function(data) {
                            if (!data) {
                                var id = $el.find("a").attr("href").match(/rapidView=(.*)&/)[1];
                                $el.append($("<a/>").attr("href", "#").text("Fix").css("margin-left", "16px").click(function(e) {
                                    e.preventDefault();
                                    $.ajax({
                                        url: contextPath + "/rest/makeadiff/1/projects/" + projectKey + "/agileboard?agileBoardId=" + id,
                                        type: "POST"
                                    });
                                }));
                            }
                        }
                    });
                });
            }
        }, 1000);
    });
})(AJS.$);
