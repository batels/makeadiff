(function($) {
    AJS.toInit(function() {
        // When an avatar is selected, allow the user to proceed
        $('.jira-avatar-picker-trigger').bind("avatar-selected", function(e, avatar, avatarSrc) {
            $(".signup-avatar-buttons-container").show();
        });

        // If an avatar has already been selected (perhaps a page load has occurred), allow the user to proceed
        var avatarId = $("img.up-d-avatar").attr("data-avatar-id");
        if (avatarId && avatarId != $("#default-avatar-id").text()) {
            $(".signup-avatar-buttons-container").show();
        }
    });
})(AJS.$);