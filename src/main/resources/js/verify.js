AJS.$(document).ready(function() {

    AJS.$(".verify").click(function() {
        var id = AJS.$(this).data("organisation-id");

        AJS.$.post(contextPath + '/rest/makeadiff/1/org/verify?id=' + id);

        location.reload();
    });

});
