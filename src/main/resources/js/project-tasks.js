(function($) {

    // Ensure that the correct agile board is rendered
    // The rapidView URL param is needed to load this page, but the JIRA Agile client side seems to ignore it when deciding which board to render
    var agileBoardId = AJS.Meta.get("agile-board-id");
    if (agileBoardId) {
        GH.RapidBoard.State.setRapidViewId(agileBoardId);
    }

    AJS.toInit(function() {
        // Prevent the rapidView param from being added to the URL
        GH.RapidBoard.UrlState.pushState = function() {};
        GH.RapidBoard.UrlState.replaceState = function() {};
    });

})(AJS.$);