(function() {
    JIRA.SkillPicker = AJS.MultiSelect.extend({
        /*_getDefaultOptions: function() {
            return AJS.$.extend(true, this._super(), {
                removeDuplicates: true,
                removeOnUnSelect: true,
                userEnteredOptionsMsg: AJS.I18n.getText("label.new")
            });
        },*/

        isValidItem: function(itemValue) {
            return true;
        }
    });

    JIRA.bind(JIRA.Events.NEW_CONTENT_ADDED, function(e, context, reason) {
        if(reason !== JIRA.CONTENT_ADDED_REASON.panelRefreshed) {
            var selector = selector || ".aui-field-skillpicker";

            AJS.$(selector, context).each(function () {
                var $fieldset = AJS.$(this).find("fieldset.skillpicker-params");
                var opts = JIRA.parseOptionsFromFieldset($fieldset),
                    issueId = opts.issueId,
                    $select = AJS.$('#' + opts.id, context),
                    data = {};

//                console.log(opts, issueId, $select);
                console.log($select);
                new JIRA.SkillPicker($select); //{
                    element: $select//,
//                    ajaxOptions: {
//                        url: contextPath + '/rest/api/1.0/skills' + (issueId ? '/' + issueId : '') + '/suggest',
//                        data: data
//                    }
//                });
            });
        }
    });
})();