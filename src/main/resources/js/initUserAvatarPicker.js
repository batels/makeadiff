(function($) {

    AJS.toInit(function() {
        JIRA.createUserAvatarPickerDialog({
            trigger: ".jira-avatar-picker-trigger",
            username: $("#avatar-owner-id").text(),
            defaultAvatarId: $("#default-avatar-id").text(),
            select: function(avatar, avatarSrc) {
                // Fire an event that consumers can listen to
                $('.jira-avatar-picker-trigger').trigger("avatar-selected", [avatar, avatarSrc]);
            }
        });

        $('.jira-avatar-picker-trigger').bind("avatar-selected", function(e, avatar, avatarSrc) {
            // Update preview, header and menu avatars
            $('.aui-avatar img').attr("src", avatarSrc);
        });

        $('.up-d-avatar').on('click', function() {
            $('.jira-avatar-picker-trigger').click();
        });
    });

})(AJS.$);
