(function($) {
    AJS.toInit(function(){
        var $form = $("#edit-project-image"),
            projectKey = $form.find("input[name='key']").val();

        $form.find("input[type='file']").change(function() {
            uploadLogo($form, $(this), projectKey);
        });

        $form.submit(function(e) {
            e.preventDefault();
            var tempFileId = $form.find("input[name='tempFileId']").val();
            if (tempFileId) {
                $.ajax({
                    url: contextPath + "/rest/makeadiff/1/project/" + projectKey + "/image/save?tempFileId=" + tempFileId,
                    type: "PUT",
                    success: function() {
                        // Take the user back to the project page
                        location.href = contextPath + "/browse/" + projectKey;
                    },
                    failure: function() {
                        // TODO
                    }
                });
            }
        });
    });

    function uploadLogo($form, $fileInput, projectKey) {
        var url = contextPath + "/rest/makeadiff/1/project/" + projectKey + "/image/upload",
            options = {
                success: function(data) {
                    $form.find("input[name='tempFileId']").val(data.tempFileId);
                    $form.find("input[type='submit']").removeAttr("disabled");
                    $(".preview-image").attr("src", contextPath + "/secure/temp-image?tempFileId=" + data.tempFileId);
                    $(".preview-image-container").show();
                },
                error: function(text, status) {
                    console.log("FAILURE");
                    console.log(text);
                    console.log(status);
                }
            };
        JIRA.AvatarUtil.uploadTemporaryAvatar(url, $fileInput, options);
    }

})(AJS.$);

