package it.com.atlassian.makeadiff.ui;

import com.atlassian.pageobjects.elements.query.Poller;
import it.com.atlassian.makeadiff.pageobjects.HomePage;
import it.com.atlassian.makeadiff.pageobjects.LoginPage;
import it.com.atlassian.makeadiff.pageobjects.ProjectOverviewPage;
import it.com.atlassian.makeadiff.pageobjects.SignupPage;
import org.junit.Test;

import static org.junit.Assert.*;

public class TestProjectChampion extends BaseTest
{
    public static final String TEST_USERNAME = "test@atlassian.com";
    public static final String TEST_NAME = "Test User";
    public static final String TEST_PASSWORD = "secretpassword";

    //todo clean up the instance roles and users?

    @Test
    public void testVolunteerChampion()
    {
        SignupPage signupPage = jira.goTo(SignupPage.class);
        signupPage.signup(TEST_USERNAME, TEST_NAME, TEST_PASSWORD);
        HomePage homePage = jira.goTo(HomePage.class);
        assertTrue(homePage.isLoggedIn());

        ProjectOverviewPage projectOverviewPage = jira.goTo(ProjectOverviewPage.class, MHP_PROJECT_KEY);
        Poller.waitUntilTrue(projectOverviewPage.hasVolunteerChampionButton());

        projectOverviewPage = projectOverviewPage.clickVolunteerChampionButton();
        Poller.waitUntilFalse(projectOverviewPage.hasVolunteerChampionButton());
    }

}
