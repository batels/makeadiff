package it.com.atlassian.makeadiff.ui;

import it.com.atlassian.makeadiff.pageobjects.LoginPage;
import org.junit.Test;
import static org.junit.Assert.assertTrue;

/**
 * WebDriver Test for {@link com.atlassian.makeadiff.action.FoundationLogin}
 */
public class TestLoginPage extends BaseTest
{
    @Test
    public void testLoginPage()
    {
        jira.goTo(LoginPage.class);
        assertTrue(jira.isAt(LoginPage.class));
    }

}
