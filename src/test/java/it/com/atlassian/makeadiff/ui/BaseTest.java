package it.com.atlassian.makeadiff.ui;

import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.pageobjects.TestedProductFactory;
import org.junit.After;
import org.junit.Before;

public class BaseTest
{
    public static final String MHP_PROJECT_KEY = "MHP";

    protected JiraTestedProduct jira;

    @Before
    public void setup()
    {
        jira = TestedProductFactory.create(JiraTestedProduct.class);
    }

    @After
    public void logout()
    {
        jira.logout();
    }

}
