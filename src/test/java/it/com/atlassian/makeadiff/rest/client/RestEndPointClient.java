package it.com.atlassian.makeadiff.rest.client;

import javax.inject.Inject;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.EnumSet;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.jaxrs.JacksonJaxbJsonProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.client.apache.config.DefaultApacheHttpClientConfig;

import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.jerseyclient.ApacheClientFactoryImpl;
import com.atlassian.jira.testkit.client.jerseyclient.JerseyClientFactory;
import com.atlassian.jira.testkit.client.restclient.AtlassianTenantFilter;
import com.atlassian.jira.testkit.client.restclient.Errors;
import com.atlassian.jira.testkit.client.restclient.Response;

import static javax.ws.rs.core.Response.Status;

/**
 * Abstract base class for REST API clients.
 */
public abstract class RestEndPointClient<T extends RestEndPointClient<T>>
{
    /**
     * Logger for this client.
     */
    private Logger log;

    /**
     * The REST plugin version to test.
     */
    public static final String REST_VERSION = "1";

    /**
     * The JerseyClientFactory used to access the REST API.
     */
    private final JerseyClientFactory clientFactory;

    /**
     * The JIRA environment data
     */
    private JIRAEnvironmentData environmentData;

    /**
     * The user to log in as.
     */
    private String loginAs = "admin";
    private String loginPassword = loginAs;

    /**
     * The version of the REST plugin to test.
     */
    private String version;

    /**
     * Lazily-instantiated Jersey client.
     */
    private Client client = null;

    /**
     * Base URL for REST module
     */
    private String baseUrl;

    /**
     * Constructs a new RestEndPointClient for a JIRA instance.
     *
     * @param environmentData The JIRA environment data
     */
    protected RestEndPointClient(JIRAEnvironmentData environmentData)
    {
        this(environmentData, REST_VERSION);
    }

    /**
     * Constructs a new RestEndPointClient for WebDriver tests. If sub-classes of this provide this constructor and use
     * the {@link javax.inject.Inject} annotation, then instances of the client can be injected to WebDriver tests.
     *
     * @param product the JIRA tested product
     */
    @Inject
    protected RestEndPointClient(JiraTestedProduct product)
    {
        this(product.getProductInstance().getBaseUrl(), REST_VERSION);
    }

    /**
     * Constructs a new RestEndPointClient for a JIRA instance.
     *
     * @param environmentData The JIRA environment data
     * @param version         a String containing the version to test against
     */
    private RestEndPointClient(JIRAEnvironmentData environmentData, String version)
    {
        this(environmentData.getBaseUrl().toExternalForm(), version);
        this.environmentData = environmentData;
    }

    protected RestEndPointClient(String baseUrl, String version)
    {
        log = LoggerFactory.getLogger(getClass());
        DefaultApacheHttpClientConfig config = new
                DefaultApacheHttpClientConfig();
        config.getProperties().put("com.sun.jersey.impl.client.httpclient.handleCookies", true);
        config.getClasses().add(JacksonJaxbJsonProvider.class);
        this.clientFactory = new ApacheClientFactoryImpl(config);
        this.baseUrl = baseUrl;
        this.version = version;
    }

    /**
     * Ensures that this client does not authenticate when making a request.
     *
     * @return this
     */
    @SuppressWarnings("unchecked")
    public T anonymous()
    {
        loginAs = null;
        loginPassword = null;
        return (T) this;
    }

    /**
     * Makes this client authenticate as the given user. If this method is not called, this client will authenticate as
     * "admin".
     *
     * @param username a String containing the username
     * @return this
     */
    public T loginAs(String username)
    {
        return loginAs(username, username);
    }

    /**
     * Makes this client authenticate as the given <tt>username</tt> and <tt>password</tt>. If this method is not
     * called, this client will authenticate as
     * "admin".
     *
     * @param username a String containing the username
     * @param password a String containing the passoword
     * @return this
     */
    @SuppressWarnings("unchecked")
    public T loginAs(String username, String password)
    {
        loginAs = username;
        loginPassword = password;
        return (T) this;
    }

    protected String getBaseUrl()
    {
        return baseUrl;
    }

    /**
     * Creates the resource that corresponds to the root of the REST API.
     *
     * @return a WebResource for the REST API root
     */
    protected WebResource createResource()
    {
        WebResource applicationResource = resourceRoot(baseUrl).path("rest").path("greenhopper").path(version);
        return addClientPath(applicationResource);
    }

    /**
     * The path to add on to the application's path to get to the client. Will be appended during {@link #createResource()}.
     *
     * @param webResource the resource
     * @return the resource with path appended
     */
    protected abstract WebResource addClientPath(WebResource webResource);

    /**
     * Creates a WebResource for the given URL. The relevant authentication parameters are added to the resource, if
     * applicable.
     *
     * @param url a String containing a URL
     * @return a WebResource, with optional authentication parameters
     */
    protected WebResource resourceRoot(String url)
    {
        WebResource resource = client().resource(url);
        if (loginAs != null)
        {
            resource = resource.queryParam("os_authType", "basic")
                    .queryParam("os_username", percentEncode(loginAs))
                    .queryParam("os_password", percentEncode(loginPassword));
        }

        return resource;
    }

    /**
     * Returns the Jersey client to use.
     *
     * @return a Client
     */
    private Client client()
    {
        if (client == null)
        {
            client = clientFactory.create();
            // NOTE: environmentData will always be null in GreenHopper tests because we use AMPS
            // if we don't care about MultiTenant testing, then we might as well remove this line.
            if (environmentData != null && StringUtils.isNotBlank(environmentData.getTenant()))
            {
                client.addFilter(new AtlassianTenantFilter(environmentData.getTenant()));
            }
        }

        return client;
    }

    protected <T> Response<T> toResponse(ClientResponse clientResponse, Class<T> entityClass)
    {
        int statusCode = clientResponse.getStatus();
        logResponse(clientResponse);
        Errors errors = null;
        T entity = null;
        if (statusCode == Status.OK.getStatusCode() && hasContent(clientResponse))
        {
            entity = extractEntityFromResponse(clientResponse, entityClass);
        }
        else if (statusCode == Status.BAD_REQUEST.getStatusCode() || statusCode == Status.NOT_FOUND.getStatusCode() || statusCode == Status.INTERNAL_SERVER_ERROR.getStatusCode())
        {
            errors = clientResponse.getEntity(Errors.class);
        }
        return new Response<T>(statusCode, errors, entity);
    }

    private <T> T extractEntityFromResponse(ClientResponse response, Class<T> entityClass)
    {
        String responseText = null;
        try
        {
            responseText = IOUtils.toString(response.getEntityInputStream());
            response.setEntityInputStream(IOUtils.toInputStream(responseText));
        }
        catch (IOException e)
        {
            throw new RuntimeException("Attempt to read response text failed", e);
        }

        try
        {
            return response.getEntity(entityClass);
        }
        catch (Exception e)
        {
            // failed to extract entity, try using gson
        }

        try
        {
            Gson gson = new Gson();
            return gson.fromJson(responseText, entityClass);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Failed to extract entity from client response", e);
        }
    }

    private boolean hasContent(ClientResponse response)
    {
        try
        {
            return response.getEntityInputStream().available() > 0;
        }
        catch (IOException e)
        {
            return false;
        }
    }


    protected <T> Response<T> get(final WebResource resource, Class<T> responseType)
    {
        return toResponse(execute(HttpMethod.GET, resource), responseType);
    }

    protected <T> Response<T> post(final WebResource resource, Class<T> responseType)
    {
        return toResponse(execute(HttpMethod.POST, resource), responseType);
    }

    protected <T> Response<T> post(final WebResource resource, Class<T> responseType, Object request)
    {
        return toResponse(execute(HttpMethod.POST, resource, request), responseType);
    }

    protected Response<Void> post(final WebResource resource, Object request)
    {
        return toResponse(execute(HttpMethod.POST, resource, request), Void.class);
    }

    protected <T> Response<T> put(final WebResource resource, Class<T> responseType)
    {
        return toResponse(execute(HttpMethod.PUT, resource), responseType);
    }

    protected Response<Void> put(final WebResource resource, Object request)
    {
        return toResponse(execute(HttpMethod.PUT, resource, request), Void.class);
    }

    protected Response<Void> put(final WebResource resource)
    {
        return toResponse(execute(HttpMethod.PUT, resource), Void.class);
    }

    protected <T> Response<T> put(final WebResource resource, Class<T> responseType, Object request)
    {
        return toResponse(execute(HttpMethod.PUT, resource, request), responseType);
    }

    protected <T> Response<T> delete(final WebResource resource, Class<T> responseType)
    {
        return toResponse(execute(HttpMethod.DELETE, resource), responseType);
    }

    protected Response<Void> delete(final WebResource resource)
    {
        return toResponse(execute(HttpMethod.DELETE, resource), Void.TYPE);
    }

    protected <T> Response<T> delete(final WebResource resource, Class<T> responseType, Object request)
    {
        return toResponse(execute(HttpMethod.DELETE, resource, request), responseType);
    }

    private ClientResponse execute(HttpMethod httpMethod, WebResource resource)
    {
        return execute(httpMethod, resource, null);
    }

    private ClientResponse execute(HttpMethod httpMethod, WebResource resource, Object request)
    {
        WebResource.Builder builder = withMediaType(resource);
        ClientResponse response = null;
        logRequest(httpMethod, resource, request);
        switch (httpMethod)
        {
            case GET:
                return builder.get(ClientResponse.class);
            case DELETE:
                return builder.delete(ClientResponse.class, request);
            case PUT:
                return builder.put(ClientResponse.class, request);
            case POST:
                return builder.post(ClientResponse.class, request);
            default:
                throw new RuntimeException("The HTTP Method [" + httpMethod + "] is not supported");
        }
    }

    private void logRequest(HttpMethod method, WebResource resource, Object request)
    {
        Gson gson = new Gson();
        String requestText = "[NO CONTENT]";
        if (request != null)
        {
            requestText = gson.toJson(request);
        }
        log.debug("===>>> " + method + " " + resource.getURI().toString() + " " + requestText);
    }

    private void logResponse(ClientResponse response)
    {
        try
        {
            // copying over the input stream to avoid EOF exception later down the road
            String responseText = IOUtils.toString(response.getEntityInputStream());
            log.debug("<<<=== " + response.getStatus() + " " + responseText);
            response.setEntityInputStream(IOUtils.toInputStream(responseText));
        }
        catch (IOException e)
        {
            log.debug("problem logging response", e);
        }
    }

    /**
     * MediaType is enforced to be APPLICATION_JSON. If you are not using JSON, think again...
     *
     * @param resource
     * @return
     */
    public WebResource.Builder withMediaType(WebResource resource)
    {
        return resource.type(MediaType.APPLICATION_JSON);
    }

    /**
     * Adds the expand query param to the given WebResource. The name of the attributes to expand must exactly match the
     * name of the enum instances that are passed in.
     *
     * @param resource a WebResource
     * @param expands  an EnumSet containing the attributes to expand
     * @return the input WebResource, with added expand parameters
     */
    protected WebResource expanded(WebResource resource, EnumSet<?> expands)
    {
        if (expands.isEmpty())
        {
            return resource;
        }

        return resource.queryParam("expand", percentEncode(StringUtils.join(expands, ",")));
    }

    /**
     * Constructs an EnumSet from a var-args param.
     *
     * @param cls    the Enum class object
     * @param expand the enum instances to expand
     * @param <E>    the Enum class
     * @return an EnumSet
     */
    protected static <E extends Enum<E>> EnumSet<E> setOf(Class<E> cls, E... expand)
    {
        return expand.length == 0 ? EnumSet.noneOf(cls) : EnumSet.of(expand[0], expand);
    }

    /**
     * Percent-encode the % when stuffing it into a query param. Otherwise it may not get escaped properly, as per <a
     * href="https://extranet.atlassian.com/x/v4Qlbw">this EAC blog</a>.
     *
     * @param queryParam the query param value
     * @return a String with % replaced by %25
     */
    protected static String percentEncode(String queryParam)
    {
        return queryParam == null ? null : queryParam.replace("%", "%25");
    }

    /**
     * TODO - if there is some library that defines these enums, please replace them. I was just trying to remove
     * our dependency on a JIRA-provided library.
     */
    private static enum HttpMethod
    {
        GET,
        POST,
        PUT,
        DELETE
    }

}
