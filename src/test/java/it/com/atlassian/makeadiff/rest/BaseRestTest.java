package it.com.atlassian.makeadiff.rest;

import org.junit.Before;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.util.TestKitLocalEnvironmentData;

public abstract class BaseRestTest
{

    TestKitLocalEnvironmentData environmentData;

    public JIRAEnvironmentData getEnvironmentData()
    {
        if (environmentData == null)
        {
            environmentData = new TestKitLocalEnvironmentData();
        }
        return environmentData;
    }

    @Before
    public void prepare()
    {
    }

}
