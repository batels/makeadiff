package it.com.atlassian.makeadiff.pageobjects;

import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Conditions;
import com.atlassian.pageobjects.elements.query.TimedCondition;

public class ProjectTasksPage extends ProjectPage
{
    @ElementBy(id = "ghx-content-main")
    private PageElement tasksContent;


    public ProjectTasksPage(String projectKey)
    {
        super(projectKey);
    }

    @Override
    public TimedCondition isAt()
    {
        return Conditions.and(tasksContent.timed().isPresent(), tasksNavItem.timed().hasClass("aui-nav-selected"));
    }

    @Override
    public String getUrl()
    {
        return "/browse/" + projectKey + "/tasks";
    }
}
