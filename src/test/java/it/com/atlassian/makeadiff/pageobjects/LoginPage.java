package it.com.atlassian.makeadiff.pageobjects;

import com.atlassian.jira.pageobjects.pages.JiraLoginPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;

/**
 * Page Object for {@link com.atlassian.makeadiff.action.FoundationLogin}
 */
public class LoginPage extends JiraLoginPage
{
    private final static String URI = "/view/login";

    @ElementBy(id = "email")
    private PageElement emailInput;

    @ElementBy(id = "password")
    private PageElement passwordInput;


    public HomePage login(String email, String password)
    {
        emailInput.type(email);
        passwordInput.type(password);
        loginButton.click();
        return pageBinder.bind(HomePage.class);
    }

    public String getUrl()
    {
        return URI;
    }

}