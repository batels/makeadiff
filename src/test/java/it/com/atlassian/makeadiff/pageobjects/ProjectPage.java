package it.com.atlassian.makeadiff.pageobjects;

import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import org.openqa.selenium.By;

public abstract class ProjectPage extends AbstractJiraPage
{

    @ElementBy(id = "overview-nav-item")
    protected PageElement overviewNavItem;

    @ElementBy(id = "tasks-nav-item")
    protected PageElement tasksNavItem;

    @ElementBy(id = "contributors-nav-item")
    protected PageElement contributorsNavItem;

    protected final String projectKey;

    public ProjectPage(String projectKey)
    {
        this.projectKey = projectKey;
    }

    public ProjectOverviewPage goToOverview()
    {
        return goTo(ProjectOverviewPage.class, overviewNavItem);
    }

    public ProjectTasksPage goToTasks()
    {
        return goTo(ProjectTasksPage.class, tasksNavItem);
    }

    public ProjectContributorsPage goToContributors()
    {
        return goTo(ProjectContributorsPage.class, contributorsNavItem);
    }

    private <T> T goTo(Class<T> pageClass, PageElement navItem) {
        navItem.find(By.tagName("a")).click();
        return pageBinder.bind(pageClass, projectKey);
    }
}
