package it.com.atlassian.makeadiff.pageobjects;

import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;

/**
 * Page Object for {@link com.atlassian.makeadiff.action.UserProfileAction}
 */
public class UserProfilePage extends AbstractJiraPage
{
    private final static String URI = "/secure/UserProfileAction.jspa";

    @ElementBy(id = "user-profile-page")
    private PageElement userProfilePage;

    @Override
    public TimedCondition isAt() {
        return userProfilePage.timed().isPresent();
    }

    @Override
    public String getUrl() {
        return URI;
    }

}
