package com.atlassian.makeadiff.ao;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.activeobjects.test.TestActiveObjects;
import com.atlassian.makeadiff.api.FoundationProject;
import com.atlassian.makeadiff.api.FoundationProjectEntity;
import net.java.ao.EntityManager;
import net.java.ao.test.junit.ActiveObjectsJUnitRunner;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(ActiveObjectsJUnitRunner.class)
public class TestAOFoundationProjectStoreImpl
{
    private EntityManager entityManager;
    private ActiveObjects activeObjects;
    private AOFoundationProjectStoreImpl projectStore;

    @Before
    public void setUp()
    {
        assertNotNull(entityManager);
        activeObjects = new TestActiveObjects(entityManager);
        projectStore = new AOFoundationProjectStoreImpl(activeObjects);

        // Prepare AO and make sure it is clear
        activeObjects.migrate(AOFoundationProjectEntity.class);
        assertEquals(0, activeObjects.find(AOFoundationProjectEntity.class).length);
    }

    @Test
    public void testAdd()
    {
        final FoundationProjectEntity project = project("TEST", "fred", FoundationProject.Status.IN_PROGRESS, null);

        projectStore.addProject(project);
        activeObjects.flushAll();

        final AOFoundationProjectEntity[] entities = activeObjects.find(AOFoundationProjectEntity.class);
        assertEquals(1, entities.length);
        assertProjectEquals(project, entities[0]);
    }

    @Test
    public void testGetByProjectKey()
    {
        final FoundationProjectEntity projectA = project("AAA", "fred", FoundationProject.Status.NEW, null);
        final FoundationProjectEntity projectB = project("BBB", "bill", FoundationProject.Status.IN_PROGRESS, null);
        final FoundationProjectEntity projectC = project("CCC", "jane", FoundationProject.Status.COMPLETED, null);
        final FoundationProjectEntity projectD = project("DDD", "kathy", FoundationProject.Status.IN_PROGRESS, "odysseus");

        projectStore.addProject(projectA);
        projectStore.addProject(projectB);
        projectStore.addProject(projectC);
        projectStore.addProject(projectD);

        activeObjects.flushAll();

        assertEquals(4, projectStore.getAllProjects().size());
        assertProjectEquals(projectA, projectStore.getProject("AAA"));
        assertProjectEquals(projectB, projectStore.getProject("BBB"));
        assertProjectEquals(projectC, projectStore.getProject("CCC"));
        assertProjectEquals(projectD, projectStore.getProject("DDD"));
    }

    private static FoundationProjectEntity project(final String key, final String creatorUserKey, final FoundationProject.Status status, final String championKey)
    {
        final FoundationProjectEntity project = new FoundationProjectEntity();
        project.setKey(key);
        project.setCreatorUserKey(creatorUserKey);
        project.setStatusKey(status.getStorageKey());

        return project;
    }

    private static void assertProjectEquals(final FoundationProjectEntity project, final AOFoundationProjectEntity entity)
    {
        assertEquals(project.getKey(), entity.getKey());
        assertEquals(project.getCreatorUserKey(), entity.getCreatorUserKey());
        assertEquals(project.getStatusKey(), entity.getStatusKey());
    }

    private static void assertProjectEquals(final FoundationProjectEntity expected, final FoundationProjectEntity actual)
    {
        assertEquals(expected.getKey(), actual.getKey());
        assertEquals(expected.getCreatorUserKey(), actual.getCreatorUserKey());
        assertEquals(expected.getStatusKey(), actual.getStatusKey());
    }
}
